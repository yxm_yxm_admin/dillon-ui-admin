package com.lw.dillon.ui.fx.view.system.logininfor;

import com.lw.dillon.ui.fx.enums.DictTypeEnum;
import com.lw.dillon.ui.fx.store.DictStore;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.loginlog.LoginLogRespVO;
import de.saxsys.mvvmfx.ViewModel;
import de.saxsys.mvvmfx.utils.mapping.ModelWrapper;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;

import java.time.LocalDateTime;
import java.util.Map;

public class LoginInforInfoViewModel implements ViewModel {

    private ModelWrapper<LoginLogRespVO> wrapper = new ModelWrapper<>();


    private Map<String, DictDataSimpleRespVO> logTypeMpa;
    private Map<String, DictDataSimpleRespVO> resultMap;

    public LoginInforInfoViewModel() {
        logTypeMpa = DictStore.getDictDataMap(DictTypeEnum.SYSTEM_LOGIN_TYPE.getDictType());
        resultMap = DictStore.getDictDataMap(DictTypeEnum.SYSTEM_LOGIN_RESULT.getDictType());
    }

    /**
     * 设置系统角色
     *
     * @param o 系统作用
     */
    public void setLoginLogRespVO(LoginLogRespVO o) {
        this.wrapper.set(o);
        this.wrapper.reload();


    }

    public LongProperty idProperty() {
        return wrapper.field("id", LoginLogRespVO::getId, LoginLogRespVO::setId);
    }

    public ObjectProperty<Integer> logTypeProperty() {
        return wrapper.field("logType", LoginLogRespVO::getLogType, LoginLogRespVO::setLogType,null);
    }

    public StringProperty usernameProperty() {
        return wrapper.field("username", LoginLogRespVO::getUsername, LoginLogRespVO::setUsername);
    }

    public StringProperty userIpProperty() {
        return wrapper.field("userIp", LoginLogRespVO::getUserIp, LoginLogRespVO::setUserIp);
    }

    public StringProperty userAgentProperty() {
        return wrapper.field("userAgent", LoginLogRespVO::getUserAgent, LoginLogRespVO::setUserAgent);
    }

    public ObjectProperty<Integer> resultProperty() {
        return wrapper.field("result", LoginLogRespVO::getResult, LoginLogRespVO::setResult,null);
    }

    public ObjectProperty<LocalDateTime> createTimeProperty() {
        return wrapper.field("createTime", LoginLogRespVO::getCreateTime, LoginLogRespVO::setCreateTime);
    }


    public Map<String, DictDataSimpleRespVO> getLogTypeMpa() {
        return logTypeMpa;
    }

    public Map<String, DictDataSimpleRespVO> getResultMap() {
        return resultMap;
    }
}
