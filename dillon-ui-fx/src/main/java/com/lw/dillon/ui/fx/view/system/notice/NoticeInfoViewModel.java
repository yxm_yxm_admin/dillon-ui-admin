package com.lw.dillon.ui.fx.view.system.notice;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.SysNoticeFeign;
import com.lw.dillon.ui.fx.vo.system.notice.NoticeCreateReqVO;
import com.lw.dillon.ui.fx.vo.system.notice.NoticeRespVO;
import com.lw.dillon.ui.fx.vo.system.notice.NoticeUpdateReqVO;
import de.saxsys.mvvmfx.ViewModel;
import de.saxsys.mvvmfx.utils.mapping.ModelWrapper;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;

public class NoticeInfoViewModel implements ViewModel {

    private ModelWrapper<NoticeRespVO> wrapper = new ModelWrapper<>();


    public NoticeInfoViewModel() {
        setNotice(new NoticeRespVO());

    }


    public void setNotice(NoticeRespVO sysDictType) {
        this.wrapper.set(sysDictType);
        this.wrapper.reload();
    }

    public void initData(Long id) {
        ProcessChain.create()

                .addSupplierInExecutor(() -> {
                    if (ObjectUtil.isNotNull(id)) {
                        return Request.connector(SysNoticeFeign.class).getNotice(id).getCheckedData();
                    } else {
                        NoticeRespVO respVO = new NoticeRespVO();
                        respVO.setStatus(0);
                        return respVO;
                    }

                })
                .addConsumerInPlatformThread(deptRespVOCommonResult -> setNotice(deptRespVOCommonResult))
                .onException(e -> e.printStackTrace())
                .run();

    }


    public StringProperty titleProperty() {
        return wrapper.field("title", NoticeRespVO::getTitle, NoticeRespVO::setTitle);
    }

    public StringProperty contentProperty() {
        return wrapper.field("content", NoticeRespVO::getContent, NoticeRespVO::setContent);
    }

    public StringProperty remarkProperty() {
        return wrapper.field("remark", NoticeRespVO::getRemark, NoticeRespVO::setRemark);
    }


    public ObjectProperty<Integer> statusProperty() {
        return wrapper.field("status", NoticeRespVO::getStatus, NoticeRespVO::setStatus, null);
    }


    public ObjectProperty<Integer> typeProperty() {
        return wrapper.field("type", NoticeRespVO::getType, NoticeRespVO::setType, null);
    }


    /**
     * 保存
     *
     * @param isEdit 是编辑
     * @return {@link Boolean}
     */
    public Boolean save(boolean isEdit) {
        wrapper.commit();

        if (isEdit) {
            NoticeUpdateReqVO reqVO = new NoticeUpdateReqVO();
            BeanUtil.copyProperties(wrapper.get(),reqVO);
            return Request.connector(SysNoticeFeign.class).updateNotice(reqVO).isSuccess();

        } else {
            NoticeCreateReqVO createReqVO = new NoticeCreateReqVO();
            BeanUtil.copyProperties(wrapper.get(),createReqVO);
            return Request.connector(SysNoticeFeign.class).createNotice(createReqVO).isSuccess();
        }

    }


    public void commitHtmlText() {
        publish("commitHtmlText");
    }
}
