package com.lw.dillon.ui.fx.vo.system.post;

import com.lw.dillon.framework.common.pojo.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PostPageReqVO extends PageParam {

    private String code;

    private String name;

    private Integer status;

}
