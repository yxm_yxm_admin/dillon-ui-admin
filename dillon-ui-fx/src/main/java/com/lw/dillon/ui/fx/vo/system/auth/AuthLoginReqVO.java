package com.lw.dillon.ui.fx.vo.system.auth;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AuthLoginReqVO {

    private String username;

    private String password;

    // ========== 图片验证码相关 ==========

    private String captchaVerification;

    // ========== 绑定社交登录时，需要传递如下参数 ==========

    private Integer socialType;

    private String socialCode;

    private String socialState;

    /**
     * 开启验证码的 Group
     */
    public interface CodeEnableGroup {}

    public boolean isSocialCodeValid() {
        return socialType == null || StrUtil.isNotEmpty(socialCode);
    }

    public boolean isSocialState() {
        return socialType == null || StrUtil.isNotEmpty(socialState);
    }

}