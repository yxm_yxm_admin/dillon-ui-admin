package com.lw.dillon.ui.fx.view.converter;

import javafx.util.StringConverter;

public class CheckBoxIntegerStringConverter extends StringConverter<Boolean> {
    @Override
    public String toString(Boolean value) {
        return value ? "1" : "0";
    }

    @Override
    public Boolean fromString(String value) {
        return "1".equals(value);
    }
}