package com.lw.dillon.ui.fx.store;

import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataSimpleRespVO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Map;

public class DictStore {

    private static Map<String, Map<String, DictDataSimpleRespVO>> dictDataMap;

    public static Map<String, Map<String, DictDataSimpleRespVO>> getAllDictDataMap() {
        return dictDataMap;
    }

    public static void setAllDictDataMap(Map<String, Map<String, DictDataSimpleRespVO>> dictDataMap) {
        DictStore.dictDataMap = dictDataMap;
    }


    public static Map<String, DictDataSimpleRespVO> getDictDataMap(String dictType) {
        return dictDataMap.get(dictType);
    }

    public static ObservableList<DictDataSimpleRespVO> getDictDataList(String dictType) {
        return FXCollections.observableArrayList(dictDataMap.get(dictType).values());
    }


    public static DictDataSimpleRespVO getDictData(String dictType, String value) {

        Map<String, DictDataSimpleRespVO> respVOMap = getDictDataMap(dictType);

        return respVOMap.get(value);
    }


}
