package com.lw.dillon.ui.fx.view.system.menu;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.SysMenuFeign;
import com.lw.dillon.ui.fx.vo.system.menu.MenuListReqVO;
import com.lw.dillon.ui.fx.vo.system.menu.MenuRespVO;
import de.saxsys.mvvmfx.SceneLifecycle;
import de.saxsys.mvvmfx.ViewModel;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;


/**
 * 菜单管理视图模型
 *
 * @author wenli
 * @date 2023/02/15
 */
public class MenuManageViewModel implements ViewModel, SceneLifecycle {
    public final static String OPEN_ALERT = "OPEN_ALERT";

    private SimpleStringProperty searchText = new SimpleStringProperty("");
    private SimpleStringProperty statusText = new SimpleStringProperty();


    private ObservableList<MenuRespVO> allData = FXCollections.observableArrayList();

    private List<MenuRespVO> menuList = CollUtil.newArrayList();

    public String getSearchText() {
        return searchText.get();
    }

    public SimpleStringProperty searchTextProperty() {
        return searchText;
    }

    public String getStatusText() {
        return statusText.get();
    }

    public SimpleStringProperty statusTextProperty() {
        return statusText;
    }

    public void initialize() {

    }

    public void someAction() {
        publish(OPEN_ALERT, "Some Error has happend");
    }


    @Override
    public void onViewAdded() {
        System.err.println("------add");
    }


    public ObservableList<MenuRespVO> getAllData() {
        return allData;
    }


    public List<MenuRespVO> getMenuList() {
        return menuList;
    }

    @Override
    public void onViewRemoved() {
        unsubscribe(OPEN_ALERT, (s, objects) -> {
        });
    }

    /**
     * 构建前端所需要树结构
     *
     * @param menus 菜单列表
     * @return 树结构列表
     */
    public List<MenuRespVO> buildMenuTree(List<MenuRespVO> menus) {
        List<MenuRespVO> returnList = new ArrayList<MenuRespVO>();
        List<Long> tempList = menus.stream().map(MenuRespVO::getId).collect(Collectors.toList());
        for (Iterator<MenuRespVO> iterator = menus.iterator(); iterator.hasNext(); ) {
            MenuRespVO menu = (MenuRespVO) iterator.next();
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(menu.getParentId())) {
                recursionFn(menus, menu);
                returnList.add(menu);
            }
        }
        if (returnList.isEmpty()) {
            returnList = menus;
        }
        return returnList;
    }

    /**
     * 递归列表
     *
     * @param list
     * @param t
     */
    private void recursionFn(List<MenuRespVO> list, MenuRespVO t) {
        // 得到子节点列表
        List<MenuRespVO> childList = getChildList(list, t);
        t.setChildren(childList);
        for (MenuRespVO tChild : childList) {
            if (hasChild(list, tChild)) {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<MenuRespVO> getChildList(List<MenuRespVO> list, MenuRespVO t) {
        List<MenuRespVO> tlist = new ArrayList<MenuRespVO>();
        Iterator<MenuRespVO> it = list.iterator();
        while (it.hasNext()) {
            MenuRespVO n = (MenuRespVO) it.next();
            if (n.getParentId().longValue() == t.getId().longValue()) {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<MenuRespVO> list, MenuRespVO t) {
        return getChildList(list, t).size() > 0;
    }


    /**
     * 获取菜单列表
     */

    public void query() {
        menuList.clear();
        String status = statusText.getValue();

        MenuListReqVO menuListReqVO = new MenuListReqVO();
        menuListReqVO.setName(searchText.getValue());
        menuListReqVO.setStatus(StrUtil.isBlank(status ) ? null : (StrUtil.equals("开启", status) ? 0 : 1));
        CommonResult<List<MenuRespVO>> commonResult = Request.connector(SysMenuFeign.class).getMenuList(menuListReqVO);
        List<MenuRespVO> MenuRespVOList = buildMenuTree(commonResult.getData());

        menuList.addAll(MenuRespVOList);
    }

    public void remove(Long menuId) {
        Request.connector(SysMenuFeign.class).deleteMenu(menuId);

    }

    public void rest() {
        searchTextProperty().setValue("");
        statusText.setValue(null);
    }


}
