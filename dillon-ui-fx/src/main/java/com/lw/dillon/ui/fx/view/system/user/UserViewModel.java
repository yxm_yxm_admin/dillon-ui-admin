package com.lw.dillon.ui.fx.view.system.user;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.dlsc.gemsfx.daterange.DateRange;
import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.SysDeptFeign;
import com.lw.dillon.ui.fx.request.feign.client.SysUserFeign;
import com.lw.dillon.ui.fx.vo.system.dept.DeptSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.user.UserPageItemRespVO;
import com.lw.dillon.ui.fx.vo.system.user.UserPageReqVO;
import com.lw.dillon.ui.fx.vo.system.user.UserUpdatePasswordReqVO;
import com.lw.dillon.ui.fx.vo.system.user.UserUpdateStatusReqVO;
import de.saxsys.mvvmfx.ViewModel;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

public class UserViewModel implements ViewModel {


    private SimpleIntegerProperty total = new SimpleIntegerProperty(0);
    private IntegerProperty pageNum = new SimpleIntegerProperty(0);
    private IntegerProperty pageSize = new SimpleIntegerProperty(10);
    private ObjectProperty<DateRange> dateRangePreset = new SimpleObjectProperty<>();
    private ObservableList<UserPageItemRespVO> userList = FXCollections.observableArrayList();

    private SimpleStringProperty status = new SimpleStringProperty();
    private BooleanProperty loading = new SimpleBooleanProperty(false);
    private StringProperty userName = new SimpleStringProperty();
    private LongProperty deptId = new SimpleLongProperty(-1);

    public void initialize() {
        deptId.addListener((obs, old, val) -> getUserPage());
    }


    public ObservableList<UserPageItemRespVO> getUserList() {
        return userList;
    }

    public String getStatus() {
        return status.get();
    }

    public SimpleStringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    public String getUserName() {
        return userName.get();
    }

    public StringProperty userNameProperty() {
        return userName;
    }

    public long getDeptId() {
        return deptId.get();
    }

    public LongProperty deptIdProperty() {
        return deptId;
    }

    public DateRange getDateRangePreset() {
        return dateRangePreset.get();
    }

    public ObjectProperty<DateRange> dateRangePresetProperty() {
        return dateRangePreset;
    }

    public void setDateRangePreset(DateRange dateRangePreset) {
        this.dateRangePreset.set(dateRangePreset);
    }


    public void reset() {
        userName.setValue(null);
        status.setValue(null);
        dateRangePreset.set(null);
        deptId.setValue(-1);
    }

    public int getTotal() {
        return total.get();
    }

    public SimpleIntegerProperty totalProperty() {
        return total;
    }

    public void setTotal(int total) {
        this.total.set(total);
    }


    /**
     * 用户列表
     */
    public void getUserPage() {
        UserPageReqVO reqVO = new UserPageReqVO();

        if (getDeptId() != -1) {
            reqVO.setDeptId(getDeptId());
        }
        reqVO.setPageNo(getPageNum() + 1);
        reqVO.setPageSize(getPageSize());
        reqVO.setUsername(getUserName());
        reqVO.setStatus(StrUtil.isBlank(status.get()) ? null : (StrUtil.equals("开启", status.get()) ? 0 : 1));
        Map<String, Object> querMap = BeanUtil.beanToMap(reqVO, false, true);
        if (ObjectUtil.isAllNotEmpty(dateRangePreset.get())) {
            String sd = dateRangePreset.get().getStartDate().atTime(0, 0, 0).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            String ed = dateRangePreset.get().getEndDate().atTime(23, 59, 59).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            querMap.put("createTime", new String[]{sd, ed});
        }

        ProcessChain.create().addRunnableInPlatformThread(()->loading.set(true))
                .addSupplierInExecutor(() -> Request.connector(SysUserFeign.class).getUserPage(querMap).getCheckedData())
                .addConsumerInPlatformThread(rel -> {
                    getUserList().clear();
                    getUserList().addAll(rel.getList());
                    setTotal(rel.getTotal().intValue());
                }).withFinal(()->loading.set(false))
                .run();
    }

    public List<DeptSimpleRespVO> getSimpleDeptList() {

        return Request.connector(SysDeptFeign.class).getSimpleDeptList().getCheckedData();
    }

    public CommonResult<Boolean> deleteUser(long menuId) {
        return Request.connector(SysUserFeign.class).deleteUser(menuId);
    }

    public CommonResult<Boolean> updateUserStatus(Integer status, Long userId) {
        UserUpdateStatusReqVO userUpdateStatusReqVO = new UserUpdateStatusReqVO();
        userUpdateStatusReqVO.setId(userId);
        userUpdateStatusReqVO.setStatus(status);
        return Request.connector(SysUserFeign.class).updateUserStatus(userUpdateStatusReqVO);
    }

    public CommonResult<Boolean> updateUserPassword(UserUpdatePasswordReqVO user) {

        return Request.connector(SysUserFeign.class).updateUserPassword(user);

    }

    public void selectAll(boolean sel) {

//        for (SysUser sysUser : getUserList()) {
//            sysUser.setSelect(sel);
//        }
    }

    public int getPageNum() {
        return pageNum.get();
    }

    public IntegerProperty pageNumProperty() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum.set(pageNum);
    }

    public int getPageSize() {
        return pageSize.get();
    }

    public IntegerProperty pageSizeProperty() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize.set(pageSize);
    }

    public boolean isLoading() {
        return loading.get();
    }

    public BooleanProperty loadingProperty() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading.set(loading);
    }
}
