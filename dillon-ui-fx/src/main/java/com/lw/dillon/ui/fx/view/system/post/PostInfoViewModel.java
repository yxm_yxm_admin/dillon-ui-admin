package com.lw.dillon.ui.fx.view.system.post;

import cn.hutool.core.bean.BeanUtil;
import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.SysPostFeign;
import com.lw.dillon.ui.fx.vo.system.post.PostCreateReqVO;
import com.lw.dillon.ui.fx.vo.system.post.PostRespVO;
import com.lw.dillon.ui.fx.vo.system.post.PostUpdateReqVO;
import de.saxsys.mvvmfx.ViewModel;
import de.saxsys.mvvmfx.utils.mapping.ModelWrapper;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;

public class PostInfoViewModel implements ViewModel {

    private ModelWrapper<PostRespVO> wrapper = new ModelWrapper<>();


    public PostInfoViewModel() {
    }

    public void initData(Long id) {
        ProcessChain.create()
                .addSupplierInExecutor(() -> {

                    if (id == null) {
                        PostRespVO postRespVO = new PostRespVO();
                        postRespVO.setStatus(0);
                        return postRespVO;
                    }
                    return Request.connector(SysPostFeign.class).getPost(id).getCheckedData();
                })
                .addConsumerInPlatformThread(result -> {
                    setPostRespVO(result);
                })
                .run();

    }

    /**
     * 设置系统角色
     *
     * @param sysPost 系统作用
     */
    public void setPostRespVO(PostRespVO sysPost) {
        this.wrapper.set(sysPost);
        this.wrapper.reload();
    }


    public ObjectProperty<Integer> postSortProperty() {
        return wrapper.field("sort", PostRespVO::getSort, PostRespVO::setSort, 0).asObject();
    }

    public StringProperty postCodeProperty() {
        return wrapper.field("code", PostRespVO::getCode, PostRespVO::setCode, "");
    }

    public ObjectProperty<Integer> statusProperty() {
        return wrapper.field("status", PostRespVO::getStatus, PostRespVO::setStatus, null);
    }

    public StringProperty postNameProperty() {
        return wrapper.field("name", PostRespVO::getName, PostRespVO::setName, "");
    }

    public StringProperty remarkProperty() {
        return wrapper.field("remark", PostRespVO::getRemark, PostRespVO::setRemark, "");
    }

    public ObjectProperty<Long> idProperty() {
        return wrapper.field("id", PostRespVO::getId, PostRespVO::setId, null);
    }

    public CommonResult<Boolean> updatePost() {
        wrapper.commit();
        PostUpdateReqVO reqVO = new PostUpdateReqVO();
        BeanUtil.copyProperties(wrapper.get(), reqVO);
        return Request.connector(SysPostFeign.class).updatePost(reqVO);
    }


    public CommonResult<Long> createPost() {
        wrapper.commit();
        PostCreateReqVO reqVO = new PostCreateReqVO();
        BeanUtil.copyProperties(wrapper.get(), reqVO);
        return Request.connector(SysPostFeign.class).createPost(reqVO);
    }


}
