package com.lw.dillon.ui.fx.view.system.post;

import atlantafx.base.controls.ModalPane;
import atlantafx.base.controls.RingProgressIndicator;
import cn.hutool.core.date.DateUtil;
import com.lw.dillon.ui.fx.util.NodeUtils;
import com.lw.dillon.ui.fx.view.control.ModalPaneDialog;
import com.lw.dillon.ui.fx.view.control.PagingControl;
import com.lw.dillon.ui.fx.vo.system.post.PostRespVO;
import de.saxsys.mvvmfx.*;
import io.datafx.core.concurrent.ProcessChain;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.kordamp.ikonli.feather.Feather;
import org.kordamp.ikonli.javafx.FontIcon;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

import static atlantafx.base.theme.Styles.*;
import static atlantafx.base.theme.Tweaks.*;

public class PostView implements FxmlView<PostViewModel>, Initializable {

    @InjectViewModel
    private PostViewModel postViewModel;


    @FXML
    private Button addBut;

    @FXML
    private TableColumn<PostRespVO, String> codeCol;

    @FXML
    private TextField codeTextField;

    @FXML
    private VBox contentPane;

    @FXML
    private TableColumn<PostRespVO, LocalDateTime> createTimeCol;

    @FXML
    private Button delBut;

    @FXML
    private Button editBut;

    @FXML
    private TableColumn<PostRespVO, Long> idCol;

    @FXML
    private TableColumn<PostRespVO, String> nameCol;

    @FXML
    private TextField nameTextField;

    @FXML
    private TableColumn<PostRespVO, String> optCol;

    @FXML
    private TableColumn<PostRespVO, String> remarkCol;

    @FXML
    private Button resetBut;

    @FXML
    private StackPane rootPane;

    @FXML
    private Button searchBut;

    @FXML
    private TableColumn<PostRespVO, Integer> sortCol;

    @FXML
    private TableColumn<PostRespVO, Integer> statusCol;

    @FXML
    private ComboBox<Integer> statusCombo;

    @FXML
    private TableView<PostRespVO> tableView;

    private RingProgressIndicator loading;
    private ModalPane modalPane;

    private PagingControl pagingControl;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        modalPane = new ModalPane();
        modalPane.setId("postModalPane");
        // reset side and transition to reuse a single modal pane between different examples
        modalPane.displayProperty().addListener((obs, old, val) -> {
            if (!val) {
                modalPane.setAlignment(Pos.CENTER);
                modalPane.usePredefinedTransitionFactories(null);
            }
        });
        rootPane.getChildren().add(modalPane);

        pagingControl = new PagingControl();
        contentPane.getChildren().add(pagingControl);
        pagingControl.totalProperty().bindBidirectional(postViewModel.totalProperty());
        postViewModel.pageNumProperty().bind(pagingControl.pageNumProperty());
        postViewModel.pageSizeProperty().bindBidirectional(pagingControl.pageSizeProperty());
        postViewModel.pageNumProperty().addListener((observable, oldValue, newValue) -> {
            postViewModel.getPostPage();
        });
        pagingControl.pageSizeProperty().addListener((observable, oldValue, newValue) -> {
            postViewModel.getPostPage();
        });
        loading = new RingProgressIndicator();
        loading.disableProperty().bind(loading.visibleProperty().not());
        loading.visibleProperty().bindBidirectional(contentPane.disableProperty());
        contentPane.disableProperty().bind(postViewModel.loadingProperty());

        rootPane.getChildren().add(loading);

        nameTextField.textProperty().bindBidirectional(postViewModel.postNameProperty());
        codeTextField.textProperty().bindBidirectional(postViewModel.postCodeProperty());
        statusCombo.valueProperty().bindBidirectional(postViewModel.statusProperty());
        statusCombo.setConverter(new StringConverter<Integer>() {
            @Override
            public String toString(Integer integer) {
                if (integer != null) {
                    return integer == 0 ? "开启" : "关闭";
                }
                return null;
            }

            @Override
            public Integer fromString(String s) {
                return null;
            }
        });
        searchBut.setOnAction(event -> postViewModel.getPostPage());
        searchBut.getStyleClass().addAll(ACCENT);

        resetBut.setOnAction(event -> postViewModel.reset());
        editBut.setOnAction(event -> {
            if (tableView.getSelectionModel().getSelectedItem() == null) {
                MvvmFX.getNotificationCenter().publish("message", 500, "请选择一条记录");
                return;
            }
            showEditPostDialog(tableView.getSelectionModel().getSelectedItem().getId());
        });
        delBut.setOnAction(event -> {
            if (tableView.getSelectionModel().getSelectedItem() == null) {
                MvvmFX.getNotificationCenter().publish("message", 500, "请选择一条记录");
                return;
            }
            showDelDialog(tableView.getSelectionModel().getSelectedItem().getId());
        });


        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        codeCol.setCellValueFactory(new PropertyValueFactory<>("code"));
        sortCol.setCellValueFactory(new PropertyValueFactory<>("sort"));
        remarkCol.setCellValueFactory(new PropertyValueFactory<>("remark"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status"));
        statusCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        var state = new Label();
                        if (item == 0) {
                            state.setText("开启");
                            state.getStyleClass().addAll(BUTTON_OUTLINED, SUCCESS);
                        } else {
                            state.setText("关闭");
                            state.getStyleClass().addAll(BUTTON_OUTLINED, DANGER);
                        }
                        HBox box = new HBox(state);
                        box.setPadding(new Insets(7, 7, 7, 7));
                        box.setAlignment(Pos.CENTER);
                        setGraphic(box);
                    }
                }
            };
        });


        optCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                    } else {

                        Button editBut = new Button("修改");
                        editBut.setOnAction(event -> showEditPostDialog(getTableRow().getItem().getId()));
                        editBut.setGraphic(FontIcon.of(Feather.EDIT));
                        editBut.getStyleClass().addAll(FLAT, ACCENT);
                        Button remBut = new Button("删除");
                        remBut.setOnAction(event -> showDelDialog(getTableRow().getItem().getId()));
                        remBut.setGraphic(FontIcon.of(Feather.TRASH));
                        remBut.getStyleClass().addAll(FLAT, ACCENT);
                        HBox box = new HBox(editBut, remBut);
                        box.setAlignment(Pos.CENTER);
                        setGraphic(box);
                    }
                }
            };
        });


        createTimeCol.setCellValueFactory(new PropertyValueFactory<>("createTime"));
        createTimeCol.setCellFactory(new Callback<TableColumn<PostRespVO, LocalDateTime>, TableCell<PostRespVO, LocalDateTime>>() {
            @Override
            public TableCell<PostRespVO, LocalDateTime> call(TableColumn<PostRespVO, LocalDateTime> param) {
                return new TableCell<>() {
                    @Override
                    protected void updateItem(LocalDateTime item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty || item == null) {
                            setText(null);
                        } else {
                            this.setText(DateUtil.format(item, "yyyy-MM-dd HH:mm:ss"));
                        }

                    }
                };
            }
        });
        tableView.setItems(postViewModel.getPostList());
        tableView.getSelectionModel().setCellSelectionEnabled(false);
        for (TableColumn c : tableView.getColumns()) {
            NodeUtils.addStyleClass(c, ALIGN_CENTER, ALIGN_LEFT, ALIGN_RIGHT);
        }

        addBut.setOnAction(event -> showAddPostDialog());

    }


    private void showEditPostDialog(Long userId) {

        ViewTuple<PostInfoView, PostInfoViewModel> load = FluentViewLoader.fxmlView(PostInfoView.class).load();
        load.getViewModel().initData(userId);


        var dialog = new ModalPaneDialog("#postModalPane", "编辑岗位", load.getView());
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create().addSupplierInExecutor(() -> load.getViewModel().updatePost()).addConsumerInPlatformThread(r -> {
                if (r.isSuccess()) {
                    dialog.close();
                    postViewModel.getPostPage();
                }
            }).onException(e -> e.printStackTrace()).run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);
    }

    private void showAddPostDialog() {

        ViewTuple<PostInfoView, PostInfoViewModel> load = FluentViewLoader.fxmlView(PostInfoView.class).load();
        load.getViewModel().initData(null);

        var dialog = new ModalPaneDialog("#postModalPane", "添加岗位", load.getView());
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create().addSupplierInExecutor(() -> load.getViewModel().createPost()).addConsumerInPlatformThread(r -> {
                if (r.isSuccess()) {
                    dialog.close();
                    postViewModel.getPostPage();
                }
            }).onException(e -> e.printStackTrace()).run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);
    }


    private void showDelDialog(Long id) {



        var dialog = new ModalPaneDialog("#postModalPane", "删除岗位", new Label("是否确认删除编号为" + id + "的岗位吗？"));
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create().addRunnableInExecutor(() -> postViewModel.deletePost(id)).addRunnableInPlatformThread(() -> {
                dialog.close();
                postViewModel.getPostPage();
            }).onException(e -> e.printStackTrace()).run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);
    }


}
