package com.lw.dillon.ui.fx.vo.system.user;

import com.lw.dillon.framework.common.validation.Mobile;
import lombok.Data;

import java.util.Set;

/**
 * 用户 Base VO，提供给添加、修改、详细的子 VO 使用
 * 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
 */
@Data
public class UserBaseVO {

    private String username;

    private String nickname;

    private String remark;

    private Long deptId;

    private Set<Long> postIds;

    private String email;

    @Mobile
    private String mobile;

    private Integer sex;

    private String avatar;

}
