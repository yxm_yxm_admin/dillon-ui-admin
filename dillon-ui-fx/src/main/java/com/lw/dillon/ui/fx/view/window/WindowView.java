package com.lw.dillon.ui.fx.view.window;

import animatefx.animation.BounceInDown;
import atlantafx.base.controls.Message;
import atlantafx.base.controls.ModalPane;
import atlantafx.base.theme.Styles;
import atlantafx.base.util.Animations;
import cn.hutool.core.util.StrUtil;
import com.lw.dillon.framework.common.exception.enums.GlobalErrorCodeConstants;
import com.lw.dillon.ui.fx.view.loginregister.LoginRegisterView;
import com.lw.dillon.ui.fx.view.loginregister.LoginRegisterViewModel;
import de.saxsys.mvvmfx.*;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.kordamp.ikonli.javafx.FontIcon;
import org.kordamp.ikonli.material2.Material2OutlinedAL;
import org.mapstruct.ap.shaded.freemarker.template.utility.StringUtil;

import java.net.URL;
import java.util.ResourceBundle;

public class WindowView implements FxmlView<WindowViewModel>, Initializable {

    @InjectViewModel
    private WindowViewModel windowViewModel;



    @FXML
    private StackPane rootPane;
    @FXML
    private StackPane contentPane;
    public static final String MAIN_MODAL_ID = "modal-pane";


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        var modalPane = new ModalPane();
        modalPane.setId(MAIN_MODAL_ID);
        // reset side and transition to reuse a single modal pane between different examples
        modalPane.displayProperty().addListener((obs, old, val) -> {
            if (!val) {
                modalPane.setAlignment(Pos.CENTER);
                modalPane.usePredefinedTransitionFactories(null);
            }
        });

        rootPane.getChildren().add(0, modalPane);
        MvvmFX.getNotificationCenter().subscribe("showMainView", (key, payload) -> {
            // trigger some actions
            Platform.runLater(() -> {
                showMainView();

            });

        });
        MvvmFX.getNotificationCenter().subscribe("showLoginRegister", (key, payload) -> {
            // trigger some actions
            Platform.runLater(() -> {
                showLoginView();

            });
        });


        MvvmFX.getNotificationCenter().subscribe("message", (key, payload) -> {
            // trigger some actions

            Platform.runLater(() -> {
                showMessage((Integer) payload[0], (String) payload[1]);

            });

        });
        MvvmFX.getNotificationCenter().subscribe("exit", (key, payload) -> {
            // trigger some actions

            Platform.runLater(() -> {
                Stage stage = (Stage) rootPane.getScene().getWindow();

                stage.close();

            });

        });

        showLoginView();

    }

    private void showMainView() {

        contentPane.getChildren().clear();
        ViewTuple<MainView, MainViewModel> load = FluentViewLoader.fxmlView(MainView.class).load();
        contentPane.getChildren().add(load.getView());
        windowViewModel.mainViewVisbleProperty().setValue(true);
    }

    private void showLoginView() {
        contentPane.getChildren().clear();
        ViewTuple<LoginRegisterView, LoginRegisterViewModel> viewTuple = FluentViewLoader.fxmlView(LoginRegisterView.class).load();
        contentPane.getChildren().add(viewTuple.getView());
        windowViewModel.mainViewVisbleProperty().setValue(false);
    }

    private void showMessage(int code, String msg) {

        if (windowViewModel.isMainViewVisble()) {
            var message = new Message(msg, null);

            if (code == GlobalErrorCodeConstants.SUCCESS.getCode()) {
                message.setTitle(StrUtil.isBlank(msg)?"操作成功":msg);
                message.setGraphic(new FontIcon(Material2OutlinedAL.CHECK_CIRCLE_OUTLINE));
                message.getStyleClass().addAll(Styles.SUCCESS);
            } else {
                message.setGraphic(new FontIcon(Material2OutlinedAL.ERROR_OUTLINE));
                message.getStyleClass().addAll(Styles.DANGER);
            }
            message.setMaxWidth(Region.USE_PREF_SIZE);
            message.setMaxHeight(45);
            StackPane.setAlignment(message, Pos.TOP_RIGHT);
            StackPane.setMargin(message, new Insets(60, 10, 0, 0));

            message.setOnClose(e -> {
                var out = Animations.slideOutUp(message, Duration.millis(250));
                out.setOnFinished(f -> rootPane.getChildren().remove(message));
                out.playFromStart();
            });

           if (!rootPane.getChildren().contains(message)) {
                rootPane.getChildren().add(message);
            }
            new BounceInDown(message).play();

            Timeline fiveSecondsWonder = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    var out = Animations.slideOutUp(message, Duration.millis(250));
                    out.setOnFinished(f -> rootPane.getChildren().remove(message));
                    out.playFromStart();
                }
            }));
            fiveSecondsWonder.play();
        }


    }
}
