package com.lw.dillon.ui.fx.view.system.dict.type;

import cn.hutool.core.bean.BeanUtil;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.SysDictTypeFeign;
import com.lw.dillon.ui.fx.vo.system.dict.type.DictTypeCreateReqVO;
import com.lw.dillon.ui.fx.vo.system.dict.type.DictTypeRespVO;
import com.lw.dillon.ui.fx.vo.system.dict.type.DictTypeUpdateReqVO;
import de.saxsys.mvvmfx.ViewModel;
import de.saxsys.mvvmfx.utils.mapping.ModelWrapper;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;

public class DictTypeInfoViewModel implements ViewModel {

    private ModelWrapper<DictTypeRespVO> sysDictTypeWrapper = new ModelWrapper<>();

    public DictTypeInfoViewModel() {
        setDictTypeRespVO(new DictTypeRespVO());

    }

    /**
     * 设置系统角色
     *
     * @param sysDictType 系统作用
     */
    public void setDictTypeRespVO(DictTypeRespVO sysDictType) {
        this.sysDictTypeWrapper.set(sysDictType);
        this.sysDictTypeWrapper.reload();
    }

    public StringProperty typeProperty() {
        return sysDictTypeWrapper.field("type", DictTypeRespVO::getType, DictTypeRespVO::setType, "0");
    }

    public StringProperty nameProperty() {
        return sysDictTypeWrapper.field("name", DictTypeRespVO::getName, DictTypeRespVO::setName, "0");
    }


    public ObjectProperty<Integer> statusProperty() {
        return sysDictTypeWrapper.field("status", DictTypeRespVO::getStatus, DictTypeRespVO::setStatus, null);
    }

    public ObjectProperty<Long> idProperty() {
        return sysDictTypeWrapper.field("id", DictTypeRespVO::getId, DictTypeRespVO::setId, 0l).asObject();
    }


    public StringProperty remarkProperty() {
        return sysDictTypeWrapper.field("remark", DictTypeRespVO::getRemark, DictTypeRespVO::setRemark, "");
    }

    public void initData(Long id) {

        ProcessChain.create()
                .addSupplierInExecutor(() -> {
                    if (id != null) {
                        return Request.connector(SysDictTypeFeign.class).getDictType(id).getCheckedData();
                    } else {
                        DictTypeRespVO respVO = new DictTypeRespVO();
                        respVO.setStatus(0);

                        return respVO;
                    }
                })
                .addConsumerInPlatformThread(rel -> setDictTypeRespVO(rel))
                .run();

    }


    /**
     * 保存
     *
     * @param isEdit 是编辑
     * @return {@link Boolean}
     */
    public Boolean save(boolean isEdit) {
        sysDictTypeWrapper.commit();
        DictTypeRespVO sysDictType = sysDictTypeWrapper.get();
        Boolean result = false;
        if (isEdit) {
            DictTypeUpdateReqVO reqVO = new DictTypeUpdateReqVO();
            BeanUtil.copyProperties(sysDictType, reqVO);
            result = Request.connector(SysDictTypeFeign.class).updateDictType(reqVO).isSuccess();
        } else {
            DictTypeCreateReqVO createReqVO = new DictTypeCreateReqVO();
            BeanUtil.copyProperties(sysDictType, createReqVO);
            result = Request.connector(SysDictTypeFeign.class).createDictType(createReqVO).isSuccess();
        }

        return result;
    }


}
