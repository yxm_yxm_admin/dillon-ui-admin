package com.lw.dillon.ui.fx.view.system.oauth2;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.enums.DictTypeEnum;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.OAuth2TokenFeign;
import com.lw.dillon.ui.fx.store.DictStore;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.token.OAuth2AccessTokenRespVO;
import de.saxsys.mvvmfx.ViewModel;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.HashMap;
import java.util.Map;

public class TokenViewModel implements ViewModel {


    private StringProperty clientId = new SimpleStringProperty();
    private StringProperty userId = new SimpleStringProperty();
    private ObjectProperty<Integer> userType = new SimpleObjectProperty<>();
    private SimpleIntegerProperty total = new SimpleIntegerProperty(0);
    private IntegerProperty pageNum = new SimpleIntegerProperty(0);
    private IntegerProperty pageSize = new SimpleIntegerProperty(10);
    private ObservableList<OAuth2AccessTokenRespVO> oAuth2AccessTokemList = FXCollections.observableArrayList();

    private BooleanProperty loading = new SimpleBooleanProperty(false);
    private Map<String, DictDataSimpleRespVO> typeMap;

    public TokenViewModel() {
        initData();
    }

    private void initData() {
        updateData();
    }

    public void updateData() {


        Map<String, Object> querMap = new HashMap<>();


        querMap.put("userId", userId.getValue());
        querMap.put("userType", userType.getValue());
        querMap.put("clientId", clientId.getValue());
        querMap.put("pageNum", pageNum.getValue() + 1);
        querMap.put("pageSize", pageSize.getValue());

        ProcessChain.create().addRunnableInPlatformThread(() -> {
                    loading.set(true);
                    oAuth2AccessTokemList.clear();
                })

                .addSupplierInExecutor(() ->
                        Request.connector(OAuth2TokenFeign.class).getAccessTokenPage(querMap).getCheckedData()
                )
                .addConsumerInPlatformThread(r -> {
                    typeMap = DictStore.getDictDataMap(DictTypeEnum.USER_TYPE.getDictType());
                    total.set(r.getTotal().intValue());
                    oAuth2AccessTokemList.addAll(r.getList());
                })
                .withFinal(()->loading.set(false))
                .onException(e -> e.printStackTrace()).run();
    }

    public CommonResult<Boolean> deleteAccessToken(String accessToken) {
        return Request.connector(OAuth2TokenFeign.class).deleteAccessToken(accessToken);
    }

    public void reset() {
        userType.setValue(null);
        userId.setValue(null);
        clientId.setValue(null);
        updateData();
    }


    public String getClientId() {
        return clientId.get();
    }

    public StringProperty clientIdProperty() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId.set(clientId);
    }

    public String getUserId() {
        return userId.get();
    }

    public StringProperty userIdProperty() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId.set(userId);
    }

    public Integer getUserType() {
        return userType.get();
    }

    public ObjectProperty<Integer> userTypeProperty() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType.set(userType);
    }

    public int getTotal() {
        return total.get();
    }

    public SimpleIntegerProperty totalProperty() {
        return total;
    }

    public void setTotal(int total) {
        this.total.set(total);
    }

    public int getPageNum() {
        return pageNum.get();
    }

    public IntegerProperty pageNumProperty() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum.set(pageNum);
    }

    public int getPageSize() {
        return pageSize.get();
    }

    public IntegerProperty pageSizeProperty() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize.set(pageSize);
    }

    public ObservableList<OAuth2AccessTokenRespVO> getoAuth2AccessTokemList() {
        return oAuth2AccessTokemList;
    }

    public void setoAuth2AccessTokemList(ObservableList<OAuth2AccessTokenRespVO> oAuth2AccessTokemList) {
        this.oAuth2AccessTokemList = oAuth2AccessTokemList;
    }

    public Map<String, DictDataSimpleRespVO> getTypeMap() {
        return typeMap;
    }

    public void setTypeMap(Map<String, DictDataSimpleRespVO> typeMap) {
        this.typeMap = typeMap;
    }

    public boolean isLoading() {
        return loading.get();
    }

    public BooleanProperty loadingProperty() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading.set(loading);
    }
}
