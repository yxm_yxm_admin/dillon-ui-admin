package com.lw.dillon.ui.fx.vo.system.dict.data;

import lombok.Data;

/**
 * 字典数据 Base VO，提供给添加、修改、详细的子 VO 使用
 * 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
 */
@Data
public class DictDataBaseVO {

    private Integer sort;

    private String label;

    private String value;

    private String dictType;

    private Integer status;

    private String colorType;
    private String cssClass;

    private String remark;

}
