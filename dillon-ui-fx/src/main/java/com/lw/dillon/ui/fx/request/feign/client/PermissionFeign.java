package com.lw.dillon.ui.fx.request.feign.client;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.feign.FeignAPI;
import com.lw.dillon.ui.fx.vo.system.permission.PermissionAssignRoleDataScopeReqVO;
import com.lw.dillon.ui.fx.vo.system.permission.PermissionAssignRoleMenuReqVO;
import com.lw.dillon.ui.fx.vo.system.permission.PermissionAssignUserRoleReqVO;
import feign.Param;
import feign.RequestLine;

import java.util.Set;

public interface PermissionFeign extends FeignAPI {

    //获得角色拥有的菜单编号
    @RequestLine("GET /system/permission/list-role-menus?roleId={roleId}")
    CommonResult<Set<Long>> getRoleMenuList(@Param("roleId") Long roleId);

    //赋予角色菜单
    @RequestLine("POST /system/permission/assign-role-menu")
    CommonResult<Boolean> assignRoleMenu(PermissionAssignRoleMenuReqVO reqVO);

    //赋予角色数据权限
    @RequestLine("POST /system/permission/assign-role-data-scope")
    CommonResult<Boolean> assignRoleDataScope(PermissionAssignRoleDataScopeReqVO reqVO);

    //获得管理员拥有的角色编号列表
    @RequestLine("GET /system/permission/list-user-roles?userId={userId}")
    CommonResult<Set<Long>> listAdminRoles(@Param("userId") Long userId);

    //赋予用户角色
    @RequestLine("POST /system/permission/assign-user-role")
    CommonResult<Boolean> assignUserRole(PermissionAssignUserRoleReqVO reqVO);

}
