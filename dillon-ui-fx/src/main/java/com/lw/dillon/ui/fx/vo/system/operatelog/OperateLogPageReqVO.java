package com.lw.dillon.ui.fx.vo.system.operatelog;

import com.lw.dillon.framework.common.pojo.PageParam;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class OperateLogPageReqVO extends PageParam {

    private String module;

    private String userNickname;

    private Integer type;

    private Boolean success;

    private LocalDateTime[] startTime;

}
