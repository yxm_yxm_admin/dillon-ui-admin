package com.lw.dillon.ui.fx.request.feign.client;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.framework.common.pojo.PageResult;
import com.lw.dillon.ui.fx.request.feign.FeignAPI;
import com.lw.dillon.ui.fx.vo.system.token.OAuth2AccessTokenRespVO;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;

import java.util.Map;

public interface OAuth2TokenFeign extends FeignAPI {


    @RequestLine("GET /system/oauth2-token/page")
//获得访问令牌分页
    CommonResult<PageResult<OAuth2AccessTokenRespVO>> getAccessTokenPage(@QueryMap Map<String, Object> quary);

    @RequestLine("DELETE /system/oauth2-token/delete?accessToken={accessToken}")
//删除访问令牌
    CommonResult<Boolean> deleteAccessToken(@Param("accessToken") String accessToken);

}
