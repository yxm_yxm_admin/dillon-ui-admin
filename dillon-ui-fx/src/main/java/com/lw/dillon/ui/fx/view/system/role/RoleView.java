package com.lw.dillon.ui.fx.view.system.role;

import atlantafx.base.controls.ModalPane;
import atlantafx.base.controls.RingProgressIndicator;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.SysRoleFeign;
import com.lw.dillon.ui.fx.util.NodeUtils;
import com.lw.dillon.ui.fx.view.control.ModalPaneDialog;
import com.lw.dillon.ui.fx.view.control.PagingControl;
import com.lw.dillon.ui.fx.vo.system.role.RoleDO;
import de.saxsys.mvvmfx.FluentViewLoader;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import de.saxsys.mvvmfx.ViewTuple;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.kordamp.ikonli.feather.Feather;
import org.kordamp.ikonli.javafx.FontIcon;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

import static atlantafx.base.theme.Styles.*;
import static atlantafx.base.theme.Tweaks.*;

public class RoleView implements FxmlView<RoleViewModel>, Initializable {

    @InjectViewModel
    private RoleViewModel viewModel;

    @FXML
    private Button addBut;

    @FXML
    private TableColumn<RoleDO, String> codeCol;

    @FXML
    private VBox contentPane;

    @FXML
    private TableColumn<RoleDO, LocalDateTime> createTimeCol;


    @FXML
    private DatePicker endDatePicker;

    @FXML
    private TableColumn<RoleDO, Long> idCol;

    @FXML
    private TableColumn<RoleDO, String> nameCol;

    @FXML
    private TableColumn<RoleDO, String> optCol;

    @FXML
    private TableColumn<RoleDO, String> remarkCol;

    @FXML
    private Button resetBut;

    @FXML
    private TextField roleSearchField;

    @FXML
    private StackPane rootPane;

    @FXML
    private Button searchBut;

    @FXML
    private TableColumn<RoleDO, Integer> sortCol;

    @FXML
    private DatePicker startDatePicker;

    @FXML
    private TableColumn<RoleDO, Boolean> statusCol;

    @FXML
    private ComboBox<Integer> statusCombo;

    @FXML
    private TableView<RoleDO> tableView;

    @FXML
    private TableColumn<RoleDO, Integer> typeCol;


    private RingProgressIndicator loading;

    private ModalPane modalPane;
    private PagingControl pagingControl;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        modalPane = new ModalPane();
        modalPane.setId("roleModalPane");
        // reset side and transition to reuse a single modal pane between different examples
        modalPane.displayProperty().addListener((obs, old, val) -> {
            if (!val) {
                modalPane.setAlignment(Pos.CENTER);
                modalPane.usePredefinedTransitionFactories(null);
            }
        });
        rootPane.getChildren().add(modalPane);

        pagingControl = new PagingControl();
        contentPane.getChildren().add(pagingControl);
        pagingControl.totalProperty().bindBidirectional(viewModel.totalProperty());
        viewModel.pageNumProperty().bind(pagingControl.pageNumProperty());
        viewModel.pageSizeProperty().bindBidirectional(pagingControl.pageSizeProperty());
        viewModel.pageNumProperty().addListener((observable, oldValue, newValue) -> {
            viewModel.queryRoleList();
        });
        pagingControl.pageSizeProperty().addListener((observable, oldValue, newValue) -> {
            viewModel.queryRoleList();
        });
        loading = new RingProgressIndicator();
        loading.disableProperty().bind(loading.visibleProperty().not());
        contentPane.disableProperty().bind(viewModel.loadingProperty());
        loading.visibleProperty().bindBidirectional(contentPane.disableProperty());
        rootPane.getChildren().add(loading);
        roleSearchField.textProperty().bindBidirectional(viewModel.nameProperty());
        startDatePicker.valueProperty().bindBidirectional(viewModel.startDateProperty());
        endDatePicker.valueProperty().bindBidirectional(viewModel.endDateProperty());
        searchBut.setOnAction(event -> viewModel.queryRoleList());
        searchBut.getStyleClass().addAll(ACCENT);

        resetBut.setOnAction(event -> viewModel.reset());

        statusCombo.valueProperty().bindBidirectional(viewModel.statusProperty());

        statusCombo.setConverter(new StringConverter<Integer>() {
            @Override
            public String toString(Integer object) {
                return ObjectUtil.equal(0, object) ? "开启" : "关闭";
            }

            @Override
            public Integer fromString(String string) {
                if (ObjectUtil.equal(string, "开启")) {
                    return 0;
                } else if (ObjectUtil.equal(string, "关闭")) {
                    return 1;
                } else {
                    return null;
                }
            }
        });


        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        typeCol.setCellValueFactory(new PropertyValueFactory<>("type"));
        codeCol.setCellValueFactory(new PropertyValueFactory<>("code"));
        sortCol.setCellValueFactory(new PropertyValueFactory<>("sort"));
        remarkCol.setCellValueFactory(new PropertyValueFactory<>("remark"));
        statusCol.setCellValueFactory(cb -> {
            var row = cb.getValue();
            var item = ObjectUtil.equal(0, row.getStatus());
            return new SimpleBooleanProperty(item);
        });
        statusCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(Boolean item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        var state = new Label();
                        if (item) {
                            state.setText("开启");
                            state.getStyleClass().addAll(BUTTON_OUTLINED, SUCCESS);
                        } else {
                            state.setText("关闭");
                            state.getStyleClass().addAll(BUTTON_OUTLINED, DANGER);
                        }
                        HBox box = new HBox(state);
                        box.setPadding(new Insets(7, 7, 7, 7));
                        box.setAlignment(Pos.CENTER);
                        setGraphic(box);
                    }
                }
            };
        });

        optCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                    } else {

                        Button editBut = new Button("修改");
                        editBut.setOnAction(event -> showRoleInfoDialog(getTableRow().getItem().getId()));
                        editBut.getStyleClass().addAll(FLAT, ACCENT);

                        Button menuBut = new Button("菜单权限");
                        menuBut.setOnAction(event -> showRAssignMenuDialog(getTableRow().getItem()));
                        menuBut.getStyleClass().addAll(FLAT, ACCENT);

                        Button dataBut = new Button("数据权限");
                        dataBut.setOnAction(event -> showAuthDataDialog(getTableRow().getItem()));
//                        dataBut.setGraphic(FontIcon.of(Feather.EDIT));
                        dataBut.getStyleClass().addAll(FLAT, ACCENT);

                        Button remBut = new Button("删除");
                        remBut.setOnAction(event -> showDelDialog(getTableRow().getItem().getId()));
                        remBut.setGraphic(FontIcon.of(Feather.TRASH));
                        remBut.getStyleClass().addAll(FLAT, DANGER);


                        HBox box = new HBox(editBut, menuBut, dataBut, remBut);
                        box.setAlignment(Pos.CENTER);
                        box.setSpacing(0);
                        setGraphic(box);
                    }
                }
            };
        });


        createTimeCol.setCellValueFactory(new PropertyValueFactory<>("createTime"));
        createTimeCol.setCellFactory(new Callback<TableColumn<RoleDO, LocalDateTime>, TableCell<RoleDO, LocalDateTime>>() {
            @Override
            public TableCell<RoleDO, LocalDateTime> call(TableColumn<RoleDO, LocalDateTime> param) {
                return new TableCell<>() {
                    @Override
                    protected void updateItem(LocalDateTime item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setText(null);
                        } else {
                            if (item != null) {
                                this.setText(DateUtil.format(item, "yyyy-MM-dd HH:mm:ss"));
                            }
                        }

                    }
                };
            }
        });
        tableView.setItems(viewModel.getRoleDOList());
        tableView.getSelectionModel().setCellSelectionEnabled(false);
        for (TableColumn<RoleDO, ?> c : tableView.getColumns()) {
            NodeUtils.addStyleClass(c, ALIGN_CENTER, ALIGN_LEFT, ALIGN_RIGHT);
        }


        addBut.setOnAction(event -> showRoleInfoDialog(null));

    }


    private void showRoleInfoDialog(Long id) {

        ViewTuple<RoleInfoView, RoleInfoViewModel> load = FluentViewLoader.fxmlView(RoleInfoView.class).load();
        load.getViewModel().getRole(id);

        var dialog = new ModalPaneDialog("#roleModalPane", ObjectUtil.isNotEmpty(id) ? "编辑角色" : "添加角色", load.getView());
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create()
                    .addSupplierInExecutor(() -> {

                        if (ObjectUtil.isNotEmpty(id)) {
                            return load.getViewModel().updateRole().isSuccess();
                        } else {
                            return load.getViewModel().createRole().isSuccess();
                        }

                    })
                    .addConsumerInPlatformThread(r -> {
                        if (r) {
                            dialog.close();
                            viewModel.queryRoleList();
                        }
                    }).onException(e -> e.printStackTrace()).run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);
    }

    private void showRAssignMenuDialog(RoleDO roleDO) {

        ViewTuple<RoleAssignMenuView, RoleAssignMenuViewModel> load = FluentViewLoader.fxmlView(RoleAssignMenuView.class).load();
        load.getViewModel().initializeData(roleDO);

        var dialog = new ModalPaneDialog("#roleModalPane", "菜单权限", load.getView());
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create()
                    .addSupplierInExecutor(() -> {

                        return load.getViewModel().assignRoleMenu();

                    })
                    .addConsumerInPlatformThread(r -> {
                        if (r.isSuccess()) {
                            dialog.close();

                        }
                    }).onException(e -> e.printStackTrace()).run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);
    }

    private void showAuthDataDialog(RoleDO roleDO) {

        ViewTuple<RoleDataPermissionView, RoleDataPermissionViewModel> load = FluentViewLoader.fxmlView(RoleDataPermissionView.class).load();
        load.getViewModel().initializeData(roleDO);


        var dialog = new ModalPaneDialog("#roleModalPane", "分配数据权限", load.getView());
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create().addSupplierInExecutor(() -> load.getViewModel().assignUserRole()).addConsumerInPlatformThread(r -> {
                if (r.isSuccess()) {
                    dialog.close();
                    viewModel.queryRoleList();
                }
            }).onException(e -> e.printStackTrace()).run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);
    }

    private void showDelDialog(Long id) {


        var dialog = new ModalPaneDialog("#roleModalPane", "删除角色",new Label("是否确认删除编号为" + id + "的角色吗？"));
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create().addSupplierInExecutor(() -> {
                return Request.connector(SysRoleFeign.class).deleteRole(id);
            }).addConsumerInPlatformThread(rel -> {
                if (rel.isSuccess()) {
                    dialog.close();
                    viewModel.queryRoleList();
                }

            }).onException(e -> e.printStackTrace()).run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);
    }


}
