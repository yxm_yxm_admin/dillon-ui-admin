package com.lw.dillon.ui.fx.view.system.role;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.lw.dillon.ui.fx.store.DictStore;
import com.lw.dillon.ui.fx.vo.system.dept.DeptSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataSimpleRespVO;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTreeCell;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class RoleDataPermissionView implements FxmlView<RoleDataPermissionViewModel>, Initializable {

    @InjectViewModel
    private RoleDataPermissionViewModel viewModel;

    @FXML
    private CheckBox expansionCheckBox;

    @FXML
    private ComboBox<DictDataSimpleRespVO> authCombox;

    @FXML
    private VBox authDataPane;

    @FXML
    private TextField roleKeyTextField;

    @FXML
    private TextField roleNameTextField;

    @FXML
    private CheckBox selectAllCheckBox;

    @FXML
    private TreeView<DeptSimpleRespVO> treeView;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        authCombox.setItems(viewModel.getDictDataSimpleRespVOS());
        authCombox.setConverter(new StringConverter<DictDataSimpleRespVO>() {
            @Override
            public String toString(DictDataSimpleRespVO dictDataSimpleRespVO) {
                if (ObjectUtil.isNotNull(dictDataSimpleRespVO)) {
                    return dictDataSimpleRespVO.getLabel();
                }
                return null;
            }

            @Override
            public DictDataSimpleRespVO fromString(String s) {
                return null;
            }
        });
        authCombox.getSelectionModel().selectedItemProperty().addListener(
                (observableValue, dictDataSimpleRespVO, t1) -> {

                    viewModel.setDataScope(t1 == null ? 1 : NumberUtil.parseInt(t1.getValue()));
                    viewModel.getDataScopeDeptIds().clear();
                });
        viewModel.dataScopeProperty().addListener((observableValue, number, t1) -> {
            DictDataSimpleRespVO sel = DictStore.getDictDataMap("system_data_scope").get(t1 + "");
            if (sel != null) {
                authCombox.getSelectionModel().select(sel);
            }
        });
        authDataPane.visibleProperty().bind(Bindings.createBooleanBinding(
                () -> {
                    DictDataSimpleRespVO sel = authCombox.getSelectionModel().selectedItemProperty().getValue();
                    if (sel != null) {
                        return StrUtil.equals(sel.getValue(), "2");
                    }
                    return false;
                }, authCombox.getSelectionModel().selectedItemProperty()
        ));
        authDataPane.managedProperty().bind(authDataPane.visibleProperty());


        roleKeyTextField.textProperty().bindBidirectional(viewModel.codeProperty());
        roleNameTextField.textProperty().bindBidirectional(viewModel.nameProperty());
        treeView.setShowRoot(false);
        treeView.setCellFactory(treeSelectTreeView -> new CheckBoxTreeCell<>() {
            @Override
            public void updateItem(DeptSimpleRespVO item, boolean empty) {
                super.updateItem(item, empty);
                if (empty||item==null) {
                    setText(null);
                } else {
                    setText(item.getName());
                }
            }
        });

        viewModel.subscribe("initData", (key, payload) -> {
            createRoot();
        });

        selectAllCheckBox.selectedProperty().addListener((observableValue, aBoolean, newVale) -> {
            treeView.getRoot().getChildren().forEach(item -> {
                if (item instanceof CheckBoxTreeItem<DeptSimpleRespVO>) {
                    ((CheckBoxTreeItem<DeptSimpleRespVO>) item).setSelected(newVale);
                }
            });
        });

        expansionCheckBox.selectedProperty().addListener((observableValue, aBoolean, newValue) -> {
            treeView.getRoot().getChildren().forEach(item -> {
                if (item instanceof CheckBoxTreeItem<DeptSimpleRespVO>) {
                    ((CheckBoxTreeItem<DeptSimpleRespVO>) item).setExpanded(newValue);
                }
            });
        });
    }


    private void createRoot() {
        DeptSimpleRespVO menuSimpleRespVO = new DeptSimpleRespVO();
        List<DeptSimpleRespVO> deptList = viewModel.getDeptList(); // Replace with your actual list
        CheckBoxTreeItem<DeptSimpleRespVO> rootItem = new CheckBoxTreeItem<>(menuSimpleRespVO);
        rootItem.setExpanded(true);
        buildTreeView(rootItem, deptList, 0); // Start building the tree recursively
        treeView.setRoot(rootItem);

    }

    private void buildTreeView(TreeItem<DeptSimpleRespVO> parentItem, List<DeptSimpleRespVO> menuList, long parentId) {
        for (DeptSimpleRespVO dept : menuList) {
            if (dept.getParentId() == parentId) {
                CheckBoxTreeItem<DeptSimpleRespVO> item = new CheckBoxTreeItem<>(dept);
                item.selectedProperty().addListener((obs, wasChecked, isNowChecked) -> {
                    if (isNowChecked) {
                        viewModel.getDataScopeDeptIds().add(item.getValue().getId());
                    } else {
                        viewModel.getDataScopeDeptIds().remove(item.getValue().getId());
                    }
                });
                item.indeterminateProperty().addListener((observableValue, aBoolean, t1) -> {
                    if (t1) {
                        viewModel.getDataScopeDeptIds().add(item.getValue().getId());
                    }
                });

                parentItem.getChildren().add(item);
                item.setSelected(viewModel.getDataScopeDeptIds().contains(dept.getId()));
                buildTreeView(item, menuList, dept.getId()); // Recursively build children
            }
        }
    }

}
