package com.lw.dillon.ui.fx.request.feign.client;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.feign.FeignAPI;
import com.lw.dillon.ui.fx.vo.system.user.UserProfileRespVO;
import com.lw.dillon.ui.fx.vo.system.user.UserProfileUpdatePasswordReqVO;
import com.lw.dillon.ui.fx.vo.system.user.UserProfileUpdateReqVO;
import feign.RequestLine;

public interface UserProfileFeign extends FeignAPI {


    //获得登录用户信息
    @RequestLine("GET /system/user/profile/get")
    CommonResult<UserProfileRespVO> profile();

    //修改用户个人信息
    @RequestLine("PUT /system/user/profile/update")
    CommonResult<Boolean> updateUserProfile(UserProfileUpdateReqVO reqVO);

    // 修改用户个人密码
    @RequestLine("PUT /system/user/profile/update-password")
    CommonResult<Boolean> updateUserProfilePassword(UserProfileUpdatePasswordReqVO reqVO);


}
