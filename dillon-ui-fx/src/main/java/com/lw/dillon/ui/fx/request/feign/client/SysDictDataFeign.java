package com.lw.dillon.ui.fx.request.feign.client;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.framework.common.pojo.PageResult;
import com.lw.dillon.ui.fx.request.feign.FeignAPI;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataCreateReqVO;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataRespVO;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataUpdateReqVO;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;

import java.util.List;
import java.util.Map;

/**
 * 数据字典信息
 *
 * @author ruoyi
 */
public interface SysDictDataFeign extends FeignAPI {
    //新增字典数据
    @RequestLine("POST /system/dict-data/create")
    CommonResult<Long> createDictData(DictDataCreateReqVO reqVO);

    //修改字典数据
    @RequestLine("PUT /system/dict-data/update")
    CommonResult<Boolean> updateDictData(DictDataUpdateReqVO reqVO);


    //删除字典数据
    @RequestLine("DELETE /system/dict-data/delete?id={id}")
    CommonResult<Boolean> deleteDictData(@Param("id") Long id);

    // 无需添加权限认证，因为前端全局都需要
//获得全部字典数据列表", description = "一般用于管理后台缓存字典数据在本地
    @RequestLine("GET /system/dict-data/list-all-simple")
    CommonResult<List<DictDataSimpleRespVO>> getSimpleDictDataList();

    ///获得字典类型的分页列表
    @RequestLine("GET /system/dict-data/page")
    CommonResult<PageResult<DictDataRespVO>> getDictTypePage(@QueryMap Map<String, Object> query);

    ///查询字典数据详细
    @RequestLine("GET /system/dict-data/get?id={id}")
    CommonResult<DictDataRespVO> getDictData(@Param("id") Long id);


}
