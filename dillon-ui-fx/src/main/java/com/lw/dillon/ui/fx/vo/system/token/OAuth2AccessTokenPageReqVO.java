package com.lw.dillon.ui.fx.vo.system.token;

import com.lw.dillon.framework.common.pojo.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class OAuth2AccessTokenPageReqVO extends PageParam {

    private Long userId;

    private Integer userType;

    private String clientId;

}
