package com.lw.dillon.ui.fx.vo.system.user;

import lombok.Data;


@Data
public class UserProfileUpdateReqVO {

    private String nickname;

    private String email;

    private String mobile;

    private Integer sex;

}
