package com.lw.dillon.ui.fx.view.system.user;

import atlantafx.base.controls.ModalPane;
import atlantafx.base.controls.PasswordTextField;
import atlantafx.base.controls.RingProgressIndicator;
import atlantafx.base.controls.ToggleSwitch;
import atlantafx.base.theme.Styles;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.extra.pinyin.PinyinUtil;
import com.dlsc.gemsfx.daterange.DateRangePicker;
import com.lw.dillon.ui.fx.util.NodeUtils;
import com.lw.dillon.ui.fx.view.control.FilterableTreeItem;
import com.lw.dillon.ui.fx.view.control.ModalPaneDialog;
import com.lw.dillon.ui.fx.view.control.PagingControl;
import com.lw.dillon.ui.fx.view.control.TreeItemPredicate;
import com.lw.dillon.ui.fx.vo.system.dept.DeptSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.user.UserPageItemRespVO;
import com.lw.dillon.ui.fx.vo.system.user.UserUpdatePasswordReqVO;
import de.saxsys.mvvmfx.FluentViewLoader;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import de.saxsys.mvvmfx.ViewTuple;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.Cursor;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import org.kordamp.ikonli.feather.Feather;
import org.kordamp.ikonli.javafx.FontIcon;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.*;

import static atlantafx.base.theme.Styles.ACCENT;
import static atlantafx.base.theme.Styles.FLAT;
import static atlantafx.base.theme.Tweaks.*;

public class UserView implements FxmlView<UserViewModel>, Initializable {

    @InjectViewModel
    private UserViewModel viewModel;

    private FilterableTreeItem<DeptSimpleRespVO> deptTreeRoot = new FilterableTreeItem<>(null);

    private ModalPane modalPane;
    private RingProgressIndicator loading;
    @FXML
    private StackPane rootPane;
    @FXML
    private ComboBox<String> statusCombo;
    @FXML
    private DateRangePicker dateRangePicker;

    @FXML
    private TextField userSearchField;
    @FXML
    private Button resetBut;
    @FXML
    private Button searchBut;
    @FXML
    private Button addBut;
    @FXML
    private HBox contentPane;
    @FXML
    private VBox tableBox;
    @FXML
    private TreeView<DeptSimpleRespVO> treeView;

    @FXML
    private TextField detpSearchField;

    @FXML
    private TableView<UserPageItemRespVO> tableView;

    @FXML
    private TableColumn<UserPageItemRespVO, String> idCol;
    @FXML
    private TableColumn<UserPageItemRespVO, String> userNameCol;
    @FXML
    private TableColumn<UserPageItemRespVO, String> nickNameCol;
    @FXML
    private TableColumn<UserPageItemRespVO, String> deptCol;
    @FXML
    private TableColumn<UserPageItemRespVO, String> phonenumberCol;
    @FXML
    private TableColumn<UserPageItemRespVO, Boolean> statusCol;
    @FXML
    private TableColumn<UserPageItemRespVO, LocalDateTime> createTimeCol;
    @FXML
    private TableColumn<UserPageItemRespVO, String> optCol;

    @FXML
    private VBox rightPane;

    private PagingControl pagingControl;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        modalPane = new ModalPane();
        modalPane.setId("userModalPane");
        // reset side and transition to reuse a single modal pane between different examples
        modalPane.displayProperty().addListener((obs, old, val) -> {
            if (!val) {
                modalPane.setAlignment(Pos.CENTER);
                modalPane.usePredefinedTransitionFactories(null);
            }
        });
        rootPane.getChildren().add(modalPane);

        dateRangePicker.setFormatter(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT));
        dateRangePicker.valueProperty().bindBidirectional(viewModel.dateRangePresetProperty());
        dateRangePicker.getDateRangeView().setPresetsLocation(Side.RIGHT);
        dateRangePicker.getDateRangeView().setApplyText("确定");
        dateRangePicker.getDateRangeView().setCancelText("取消");
        dateRangePicker.getDateRangeView().setPresetTitle("快速选择");

        pagingControl = new PagingControl();
        tableBox.getChildren().add(pagingControl);
        pagingControl.totalProperty().bind(viewModel.totalProperty());
        viewModel.pageNumProperty().bind(pagingControl.pageNumProperty());
        viewModel.pageSizeProperty().bind(pagingControl.pageSizeProperty());
        pagingControl.pageNumProperty().addListener((observable, oldValue, newValue) -> {
            queryUser();
        });

        pagingControl.pageSizeProperty().addListener((observable, oldValue, newValue) -> {
            queryUser();
        });
        loading = new RingProgressIndicator();
        loading.disableProperty().bind(loading.visibleProperty().not());
        loading.visibleProperty().bindBidirectional(contentPane.disableProperty());
        contentPane.disableProperty().bind(viewModel.loadingProperty());
        rootPane.getChildren().add(loading);

        treeView.setShowRoot(false);
        deptTreeRoot.predicateProperty().bind(Bindings.createObjectBinding(() -> {
            if (detpSearchField.getText() == null || detpSearchField.getText().isEmpty()) {
                return null;
            }
            return TreeItemPredicate.create(actor -> {
                if (containsString(actor.getName(), detpSearchField.getText())) {

                    return true;
                } else {
                    return false;
                }
            });
        }, detpSearchField.textProperty()));
        treeView.setRoot(deptTreeRoot);

        userSearchField.textProperty().bindBidirectional(viewModel.userNameProperty());
        statusCombo.valueProperty().bindBidirectional(viewModel.statusProperty());

        searchBut.setOnAction(event -> queryUser());
        resetBut.setOnAction(event -> viewModel.reset());
        searchBut.getStyleClass().addAll(ACCENT);


        treeView.setCellFactory(new Callback<TreeView<DeptSimpleRespVO>, TreeCell<DeptSimpleRespVO>>() {
            @Override
            public TreeCell<DeptSimpleRespVO> call(TreeView<DeptSimpleRespVO> param) {
                return new TreeCell<>() {
                    @Override
                    protected void updateItem(DeptSimpleRespVO item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setText(null);
                        } else {
                            setText(item.getName());
                        }
                    }
                };
            }
        });
        treeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {

            if (newValue != null) {
                viewModel.deptIdProperty().setValue(newValue.getValue().getId());
                queryUser();
            }

        });
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        userNameCol.setCellValueFactory(new PropertyValueFactory<>("username"));
        nickNameCol.setCellValueFactory(new PropertyValueFactory<>("nickname"));
        deptCol.setCellValueFactory(cb -> {
            var row = cb.getValue();
            var dept = row.getDept();

            return new SimpleStringProperty(ObjectUtil.isEmpty(dept) ? "" : dept.getName());
        });

        statusCol.setCellValueFactory(cb -> {
            var row = cb.getValue();
            var item = ObjectUtil.equal(0, row.getStatus());
            return new SimpleBooleanProperty(item);
        });
        statusCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(Boolean item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        ToggleSwitch state = new ToggleSwitch();
                        state.pseudoClassStateChanged(Styles.STATE_SUCCESS, item);
                        state.setSelected(item);
                        state.selectedProperty().addListener((observableValue, old, val) -> {

                            showUpdateUserStatusDialog(getTableRow().getItem().getId(), getTableRow().getItem().getUsername(), val, state);
                        });

                        setGraphic(state);
                    }
                }
            };
        });

        optCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                    } else {

                        Button editBut = new Button("修改");
                        editBut.setOnAction(event -> showEditDialog(getTableRow().getItem().getId()));
                        editBut.setGraphic(FontIcon.of(Feather.EDIT));
                        editBut.getStyleClass().addAll(FLAT, ACCENT);
                        Button remBut = new Button("删除");
                        remBut.setOnAction(event -> showDelDialog(getTableRow().getItem().getId()));
                        remBut.setGraphic(FontIcon.of(Feather.TRASH));
                        remBut.getStyleClass().addAll(FLAT, ACCENT);

                        MenuItem resetPwdItem = new MenuItem("重置密码");
                        resetPwdItem.setOnAction(event -> showResetPwdDialog(getTableRow().getItem()));
                        MenuItem assignRolesItme = new MenuItem("分配角色");
                        assignRolesItme.setOnAction(event -> showAuthRoleDialog(getTableRow().getItem()));
                        MenuButton moreBut = new MenuButton("更多");
                        moreBut.getItems().addAll(resetPwdItem, assignRolesItme);
                        moreBut.getStyleClass().addAll(FLAT, ACCENT);
                        HBox box = new HBox(editBut, remBut, moreBut);
                        box.setAlignment(Pos.CENTER);
//                            box.setSpacing(7);
                        setGraphic(box);
                    }
                }
            };
        });


        phonenumberCol.setCellValueFactory(new PropertyValueFactory<>("mobile"));
        createTimeCol.setCellValueFactory(new PropertyValueFactory<>("createTime"));
        createTimeCol.setCellFactory(new Callback<TableColumn<UserPageItemRespVO, LocalDateTime>, TableCell<UserPageItemRespVO, LocalDateTime>>() {
            @Override
            public TableCell<UserPageItemRespVO, LocalDateTime> call(TableColumn<UserPageItemRespVO, LocalDateTime> param) {
                return new TableCell<>() {
                    @Override
                    protected void updateItem(LocalDateTime item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setText(null);
                        } else {
                            if (item != null) {
                                this.setText(DateUtil.format(item, "yyyy-MM-dd HH:mm:ss"));
                            }
                        }

                    }
                };
            }
        });
        tableView.setItems(viewModel.getUserList());
        tableView.getSelectionModel().setCellSelectionEnabled(false);
        for (TableColumn<?, ?> c : tableView.getColumns()) {
            NodeUtils.addStyleClass(c, ALIGN_CENTER, ALIGN_LEFT, ALIGN_RIGHT);
        }


        initListener();
        initData();
    }

    private void initListener() {

        addBut.setOnAction(event -> showAddDialog());
    }


    /**
     * 创建根
     */
    private void createDeptTree(List<DeptSimpleRespVO> deptList) {
        Map<Long, FilterableTreeItem<DeptSimpleRespVO>> treeItemMap = new HashMap<>();

        deptTreeRoot.getInternalChildren().clear();
        // Build the tree structure
        for (DeptSimpleRespVO dept : deptList) {
            FilterableTreeItem<DeptSimpleRespVO> treeItem = new FilterableTreeItem<>(dept);
            treeItemMap.put(dept.getId(), treeItem);

            long parentId = dept.getParentId();
            if (parentId == 0) {
                deptTreeRoot.getInternalChildren().add(treeItem);
            } else {
                FilterableTreeItem<DeptSimpleRespVO> parentItem = treeItemMap.get(parentId);
                if (parentItem != null) {
                    parentItem.getInternalChildren().add(treeItem);
                }
            }
        }

    }

    private boolean containsString(String s1, String s2) {

        return PinyinUtil.getFirstLetter(s1, "").toLowerCase(Locale.ROOT).contains(PinyinUtil.getFirstLetter(s2, "").toLowerCase(Locale.ROOT));
    }

    private void showDelDialog(long userId) {

        var dialog = new ModalPaneDialog("#userModalPane", "删除用户", new Label("是否确认删除编号为" + userId + "的用户吗？"));
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            remove(userId);
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);
    }

    private void showUpdateUserStatusDialog(Long userId, String userName, Boolean sel, ToggleSwitch status) {


        var dialog = new ModalPaneDialog("#userModalPane", "修改状态", new Label("是否确认停用【" + userName + "】用户吗？"));
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create().addRunnableInExecutor(() -> viewModel.updateUserStatus(sel ? 0 : 1, userId)).addRunnableInPlatformThread(() -> {
                status.pseudoClassStateChanged(Styles.STATE_SUCCESS, sel);
                status.setSelected(sel);
                dialog.close();
            }).onException(e -> e.printStackTrace()).run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);
    }


    /**
     * 显示编辑对话框
     */
    private void showEditDialog(Long userId) {

        ViewTuple<UserInfoView, UserInfoViewModel> load = FluentViewLoader.fxmlView(UserInfoView.class).load();
        load.getViewModel().initData(userId);


        var dialog = new ModalPaneDialog("#userModalPane", "编辑用户", load.getView());
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create().addSupplierInExecutor(() -> load.getViewModel().updateUser()).addConsumerInPlatformThread(r -> {
                if (r.isSuccess()) {
                    dialog.close();
                    queryUser();
                }
            }).onException(e -> e.printStackTrace()).run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);
    }

    private void showAddDialog() {

        ViewTuple<UserInfoView, UserInfoViewModel> load = FluentViewLoader.fxmlView(UserInfoView.class).load();
        load.getViewModel().initData(null);


        var dialog = new ModalPaneDialog("#userModalPane", "添加用户", load.getView());
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create().addSupplierInExecutor(() -> load.getViewModel().addUser()).addConsumerInPlatformThread(r -> {
                if (r.isSuccess()) {
                    dialog.close();
                    queryUser();
                }
            }).onException(e -> e.printStackTrace()).run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);
    }

    /**
     * 显示复位pwd对话框
     *
     * @param sysUser 系统用户
     */
    private void showResetPwdDialog(UserPageItemRespVO sysUser) {

        var passwordField = new PasswordTextField("");

        var icon = new FontIcon(Feather.EYE_OFF);
        icon.setCursor(Cursor.HAND);
        icon.setOnMouseClicked(e -> {
            icon.setIconCode(passwordField.getRevealPassword() ? Feather.EYE_OFF : Feather.EYE);
            passwordField.setRevealPassword(!passwordField.getRevealPassword());
        });
        passwordField.setRight(icon);

        VBox vBox = new VBox(new Label("请输入\"" + sysUser.getUsername() + "\"的新密码"), passwordField);
        vBox.setSpacing(10);
        vBox.setPadding(new Insets(10, 10, 10, 10));


        var dialog = new ModalPaneDialog("#userModalPane", "重置密码", vBox);
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create().addSupplierInPlatformThread(() -> {
                UserUpdatePasswordReqVO newUserPageItemRespVO = new UserUpdatePasswordReqVO();
                newUserPageItemRespVO.setId(sysUser.getId());
                newUserPageItemRespVO.setPassword(passwordField.getPassword());

                return newUserPageItemRespVO;
            }).addConsumerInExecutor(rel -> viewModel.updateUserPassword(rel)).addRunnableInPlatformThread(() -> {
                dialog.close();
            }).onException(e -> e.printStackTrace()).run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);


    }

    private void showAuthRoleDialog(UserPageItemRespVO sysUser) {

        ViewTuple<AuthRoleView, AuthRoleViewModel> load = FluentViewLoader.fxmlView(AuthRoleView.class).load();
        load.getViewModel().setUserName(sysUser.getUsername());
        load.getViewModel().setNickName(sysUser.getNickname());
        load.getViewModel().initData(sysUser.getId());

        var dialog = new ModalPaneDialog("#userModalPane", "分配角色", load.getView());
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create().addSupplierInExecutor(() -> load.getViewModel().save()).addConsumerInPlatformThread(res -> {

                if (res.isSuccess()) {
                    dialog.close();
                }
            }).onException(e -> e.printStackTrace()).run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);


    }


    /**
     * 删除
     *
     * @param userId 用户id
     */
    private void remove(Long userId) {

        ProcessChain.create().addRunnableInExecutor(() -> viewModel.deleteUser(userId)).addRunnableInPlatformThread(() -> {
            modalPane.hide();
            queryUser();
        }).onException(e -> e.printStackTrace()).run();

    }


    private void queryUser() {
        viewModel.getUserPage();
    }

    private void initData() {
        ProcessChain.create().addSupplierInExecutor(() -> {
                    viewModel.getUserPage();
                    return viewModel.getSimpleDeptList();

                }).addConsumerInPlatformThread(rel -> createDeptTree(rel))
                .onException(e -> e.printStackTrace()).run();
    }

}
