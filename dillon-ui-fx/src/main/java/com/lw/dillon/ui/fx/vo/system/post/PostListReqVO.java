package com.lw.dillon.ui.fx.vo.system.post;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PostListReqVO extends PostBaseVO {

    private String name;

    private Integer status;

}
