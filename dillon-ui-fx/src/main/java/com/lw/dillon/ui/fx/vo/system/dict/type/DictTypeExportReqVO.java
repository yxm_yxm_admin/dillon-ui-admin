package com.lw.dillon.ui.fx.vo.system.dict.type;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class DictTypeExportReqVO {

    private String name;

    private String type;

    private Integer status;

    private LocalDateTime[] createTime;

}
