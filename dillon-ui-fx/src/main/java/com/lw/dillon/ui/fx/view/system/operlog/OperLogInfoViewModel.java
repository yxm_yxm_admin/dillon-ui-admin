package com.lw.dillon.ui.fx.view.system.operlog;

import com.lw.dillon.ui.fx.vo.system.operatelog.OperateLogRespVO;
import de.saxsys.mvvmfx.ViewModel;
import de.saxsys.mvvmfx.utils.mapping.ModelWrapper;
import javafx.beans.property.*;

import java.time.LocalDateTime;
import java.util.HashMap;

public class OperLogInfoViewModel implements ViewModel {

    private ModelWrapper<OperateLogRespVO> sysDictWrapper = new ModelWrapper<>();

    private StringProperty requestMethodUrl = new SimpleStringProperty();

    public OperLogInfoViewModel() {

    }

    /**
     * 设置系统角色
     *
     * @param sysDict 系统作用
     */
    public void setOperateLogRespVO(OperateLogRespVO sysDict) {
        sysDict.setExts(new HashMap<>());
        requestMethodUrl.set(sysDict.getRequestMethod() + " " + sysDict.getRequestUrl());
        this.sysDictWrapper.set(sysDict);
        this.sysDictWrapper.reload();
    }

    public StringProperty moduleProperty() {
        return sysDictWrapper.field("module", OperateLogRespVO::getModule, OperateLogRespVO::setModule);
    }

    public StringProperty nameProperty() {
        return sysDictWrapper.field("name", OperateLogRespVO::getName, OperateLogRespVO::setName);
    }

    public StringProperty userIpProperty() {
        return sysDictWrapper.field("userIp", OperateLogRespVO::getUserIp, OperateLogRespVO::setUserIp);
    }

    public StringProperty userNicknameProperty() {
        return sysDictWrapper.field("userNickname", OperateLogRespVO::getUserNickname, OperateLogRespVO::setUserNickname);
    }

    public StringProperty requestUrlProperty() {
        return sysDictWrapper.field("requestUrl", OperateLogRespVO::getRequestUrl, OperateLogRespVO::setRequestUrl);
    }

    public StringProperty requestMethodProperty() {
        return sysDictWrapper.field("requestMethod", OperateLogRespVO::getRequestMethod, OperateLogRespVO::setRequestMethod);
    }

    public StringProperty javaMethodProperty() {
        return sysDictWrapper.field("javaMethod", OperateLogRespVO::getJavaMethod, OperateLogRespVO::setJavaMethod);
    }

    public StringProperty javaMethodArgsProperty() {
        return sysDictWrapper.field("javaMethodArgs", OperateLogRespVO::getJavaMethodArgs, OperateLogRespVO::setJavaMethodArgs);
    }

    public StringProperty resultDataProperty() {
        return sysDictWrapper.field("resultData", OperateLogRespVO::getResultData, OperateLogRespVO::setResultData);
    }

    public StringProperty resultMsgProperty() {
        return sysDictWrapper.field("resultMsg", OperateLogRespVO::getResultMsg, OperateLogRespVO::setResultMsg);
    }

    public ObjectProperty<LocalDateTime> startTimeProperty() {
        return sysDictWrapper.field("startTime", OperateLogRespVO::getStartTime, OperateLogRespVO::setStartTime);
    }

    public IntegerProperty durationProperty() {
        return sysDictWrapper.field("duration", OperateLogRespVO::getDuration, OperateLogRespVO::setDuration);
    }

    public IntegerProperty resultCodeProperty() {
        return sysDictWrapper.field("resultCode", OperateLogRespVO::getResultCode, OperateLogRespVO::setResultCode);
    }

    public StringProperty contentCodeProperty() {
        return sysDictWrapper.field("content", OperateLogRespVO::getContent, OperateLogRespVO::setContent);
    }

    public LongProperty idProperty() {
        return sysDictWrapper.field("id", OperateLogRespVO::getId, OperateLogRespVO::setId);
    }

    public LongProperty userIdProperty() {
        return sysDictWrapper.field("userId", OperateLogRespVO::getUserId, OperateLogRespVO::setUserId);
    }

    public StringProperty traceIdProperty() {
        return sysDictWrapper.field("traceId", OperateLogRespVO::getTraceId, OperateLogRespVO::setTraceId);
    }

    public StringProperty userAgentProperty() {
        return sysDictWrapper.field("userAgent", OperateLogRespVO::getUserAgent, OperateLogRespVO::setUserAgent);
    }

    public MapProperty<String, Object> extsProperty() {
        return sysDictWrapper.field("exts", OperateLogRespVO::getExts, OperateLogRespVO::setExts);
    }

    public String getRequestMethodUrl() {
        return requestMethodUrl.get();
    }

    public StringProperty requestMethodUrlProperty() {
        return requestMethodUrl;
    }

    public void setRequestMethodUrl(String requestMethodUrl) {
        this.requestMethodUrl.set(requestMethodUrl);
    }
}
