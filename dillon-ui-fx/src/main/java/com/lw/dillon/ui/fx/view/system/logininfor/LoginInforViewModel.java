package com.lw.dillon.ui.fx.view.system.logininfor;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import com.lw.dillon.ui.fx.enums.DictTypeEnum;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.SysLogininforFeign;
import com.lw.dillon.ui.fx.store.DictStore;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.loginlog.LoginLogRespVO;
import de.saxsys.mvvmfx.ViewModel;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class LoginInforViewModel implements ViewModel {


    private SimpleIntegerProperty total = new SimpleIntegerProperty(0);
    private ObjectProperty<LocalDate> startDate = new SimpleObjectProperty<>();
    private ObjectProperty<LocalDate> endDate = new SimpleObjectProperty();
    private ObjectProperty<Boolean> status = new SimpleObjectProperty<>();
    private StringProperty ipaddr = new SimpleStringProperty();
    private StringProperty userName = new SimpleStringProperty();

    private IntegerProperty pageNum = new SimpleIntegerProperty(0);
    private IntegerProperty pageSize = new SimpleIntegerProperty(10);

    private ObservableList<LoginLogRespVO> loginLogList = FXCollections.observableArrayList();
    private Map<String, DictDataSimpleRespVO> typeMap;
    private Map<String, DictDataSimpleRespVO> resultMap;

    public LoginInforViewModel() {
        queryLogininforList();
    }

    private BooleanProperty loding = new SimpleBooleanProperty(false);

    public void queryLogininforList() {


        Map<String, Object> querMap = new HashMap<>();
        if (ObjectUtil.isAllNotEmpty(startDate.getValue(), endDate.getValue())) {
            querMap.put("startTime", new String[]{
                    startDate.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + " 00:00:00",
                    endDate.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + " 23:59:59"
            });
        }

        querMap.put("userIp", ipaddr.getValue());
        querMap.put("username", userName.getValue());
        querMap.put("status", status.getValue());
        querMap.put("pageNum", pageNum.getValue() + 1);
        querMap.put("pageSize", pageSize.getValue());

        ProcessChain.create().addRunnableInPlatformThread(() -> {
                    loding.set(true);
                    loginLogList.clear();
                })
                .addSupplierInExecutor(() ->
                        Request.connector(SysLogininforFeign.class).getLoginLogPage(querMap).getCheckedData()
                )
                .addConsumerInPlatformThread(r -> {
                    typeMap = DictStore.getDictDataMap(DictTypeEnum.SYSTEM_LOGIN_TYPE.getDictType());
                    resultMap = DictStore.getDictDataMap(DictTypeEnum.SYSTEM_LOGIN_RESULT.getDictType());
                    total.setValue(NumberUtil.parseInt(r.getTotal() + ""));
                    loginLogList.addAll(r.getList());
                }).withFinal(() -> loding.set(false))

                .onException(e -> e.printStackTrace()).run();


    }

    public void reset() {
        ipaddr.setValue(null);
        userName.setValue(null);
        status.setValue(null);
        endDate.setValue(null);
        startDate.setValue(null);
        queryLogininforList();
    }


    public int getTotal() {
        return total.get();
    }

    public SimpleIntegerProperty totalProperty() {
        return total;
    }

    public void setTotal(int total) {
        this.total.set(total);
    }

    public LocalDate getStartDate() {
        return startDate.get();
    }

    public ObjectProperty<LocalDate> startDateProperty() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate.set(startDate);
    }

    public LocalDate getEndDate() {
        return endDate.get();
    }

    public ObjectProperty<LocalDate> endDateProperty() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate.set(endDate);
    }

    public Boolean getStatus() {
        return status.get();
    }

    public ObjectProperty<Boolean> statusProperty() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status.set(status);
    }

    public String getIpaddr() {
        return ipaddr.get();
    }

    public StringProperty ipaddrProperty() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr.set(ipaddr);
    }

    public String getUserName() {
        return userName.get();
    }

    public StringProperty userNameProperty() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName.set(userName);
    }

    public int getPageNum() {
        return pageNum.get();
    }

    public IntegerProperty pageNumProperty() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum.set(pageNum);
    }

    public int getPageSize() {
        return pageSize.get();
    }

    public IntegerProperty pageSizeProperty() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize.set(pageSize);
    }

    public ObservableList<LoginLogRespVO> getLoginLogList() {
        return loginLogList;
    }

    public void setLoginLogList(ObservableList<LoginLogRespVO> loginLogList) {
        this.loginLogList = loginLogList;
    }

    public Map<String, DictDataSimpleRespVO> getTypeMap() {
        return typeMap;
    }

    public Map<String, DictDataSimpleRespVO> getResultMap() {
        return resultMap;
    }

    public boolean isLoding() {
        return loding.get();
    }

    public BooleanProperty lodingProperty() {
        return loding;
    }

    public void setLoding(boolean loding) {
        this.loding.set(loding);
    }
}
