package com.lw.dillon.ui.fx.view.system.user;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.PermissionFeign;
import com.lw.dillon.ui.fx.request.feign.client.SysRoleFeign;
import com.lw.dillon.ui.fx.vo.system.permission.PermissionAssignUserRoleReqVO;
import com.lw.dillon.ui.fx.vo.system.role.RoleSimpleRespVO;
import de.saxsys.mvvmfx.ViewModel;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;

import java.util.HashSet;
import java.util.Set;

public class AuthRoleViewModel implements ViewModel {

    private StringProperty userName = new SimpleStringProperty();
    private StringProperty nickName = new SimpleStringProperty();
    private Long userId = null;

    private Set<Long> userRoleSet = new HashSet<>();

    private ObservableList<RoleSimpleRespVO> roleSimpleRespVOList = FXCollections.observableArrayList();
    private ObservableSet<Long> selRoleSet = FXCollections.observableSet();

    public void initialize() {

    }

    public String getUserName() {
        return userName.get();
    }

    public StringProperty userNameProperty() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName.set(userName);
    }

    public String getNickName() {
        return nickName.get();
    }

    public StringProperty nickNameProperty() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName.set(nickName);
    }

    public Set<Long> getUserRoleSet() {
        return userRoleSet;
    }

    public void setUserRoleSet(Set<Long> userRoleSet) {
        this.userRoleSet = userRoleSet;
    }

    public ObservableList<RoleSimpleRespVO> getRoleSimpleRespVOList() {
        return roleSimpleRespVOList;
    }

    public ObservableSet<Long> getSelRoleSet() {
        return selRoleSet;
    }

    public void setSelRoleSet(ObservableSet<Long> selRoleSet) {
        this.selRoleSet = selRoleSet;
    }

    public void initData(Long userId) {
        this.userId=userId;
        ProcessChain.create()
                .addSupplierInExecutor(() -> {
                    return Request.connector(PermissionFeign.class).listAdminRoles(userId).getCheckedData();
                })
                .addConsumerInPlatformThread(rel -> setUserRoleSet(rel))
                .addSupplierInExecutor(
                        () -> Request.connector(SysRoleFeign.class).getSimpleRoleList().getCheckedData())
                .addConsumerInPlatformThread(rel -> {
                    roleSimpleRespVOList.clear();
                    roleSimpleRespVOList.setAll(rel);
                    publish("initData");
                })
                .onException(e -> e.printStackTrace())
                .run();
    }

    public CommonResult<Boolean> save() {
        PermissionAssignUserRoleReqVO reqVO = new PermissionAssignUserRoleReqVO();
        reqVO.setUserId(userId);
        reqVO.setRoleIds(selRoleSet);
        return Request.connector(PermissionFeign.class).assignUserRole(reqVO);
    }
}
