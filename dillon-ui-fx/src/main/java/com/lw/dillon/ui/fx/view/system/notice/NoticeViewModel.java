package com.lw.dillon.ui.fx.view.system.notice;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.SysNoticeFeign;
import com.lw.dillon.ui.fx.vo.system.notice.NoticeRespVO;
import de.saxsys.mvvmfx.ViewModel;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.HashMap;
import java.util.Map;

public class NoticeViewModel implements ViewModel {


    private SimpleIntegerProperty total = new SimpleIntegerProperty(0);
    private StringProperty title = new SimpleStringProperty();
    private ObjectProperty<Integer> status = new SimpleObjectProperty<>();
    private IntegerProperty pageNum = new SimpleIntegerProperty(0);
    private IntegerProperty pageSize = new SimpleIntegerProperty(10);
    private BooleanProperty loading = new SimpleBooleanProperty(false);

    private ObservableList<NoticeRespVO> noticeList = FXCollections.observableArrayList();

    public NoticeViewModel() {
        initData();
    }

    private void initData() {
        updateData();
    }

    public void updateData() {


        Map<String, Object> querMap = new HashMap<>();

        querMap.put("title", title.getValue());
        querMap.put("status", status.getValue());
        querMap.put("pageNum", pageNum.getValue() + 1);
        querMap.put("pageSize", pageSize.getValue());

        ProcessChain.create().addRunnableInPlatformThread(() -> {
                    loading.set(true);
                    noticeList.clear();
                })
                .addSupplierInExecutor(() ->
                        Request.connector(SysNoticeFeign.class).getNoticePage(querMap).getCheckedData()
                )
                .addConsumerInPlatformThread(r -> {
                    total.set(r.getTotal().intValue());
                    noticeList.addAll(r.getList());
                })
                .withFinal(() -> loading.set(false))
                .onException(e -> e.printStackTrace()).run();


    }

    public CommonResult deleteNotice(Long id) {
        return Request.connector(SysNoticeFeign.class).deleteNotice(id);
    }

    public void reset() {
        title.setValue(null);
        status.setValue(null);
        updateData();
    }


    public int getTotal() {
        return total.get();
    }

    public SimpleIntegerProperty totalProperty() {
        return total;
    }

    public void setTotal(int total) {
        this.total.set(total);
    }

    public String getTitle() {
        return title.get();
    }

    public StringProperty titleProperty() {
        return title;
    }

    public void setTitle(String title) {
        this.title.set(title);
    }


    public Integer getStatus() {
        return status.get();
    }

    public ObjectProperty<Integer> statusProperty() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status.set(status);
    }

    public int getPageNum() {
        return pageNum.get();
    }

    public IntegerProperty pageNumProperty() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum.set(pageNum);
    }

    public int getPageSize() {
        return pageSize.get();
    }

    public IntegerProperty pageSizeProperty() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize.set(pageSize);
    }

    public ObservableList<NoticeRespVO> getNoticeList() {
        return noticeList;
    }

    public void setNoticeList(ObservableList<NoticeRespVO> noticeList) {
        this.noticeList = noticeList;
    }

    public boolean isLoading() {
        return loading.get();
    }

    public BooleanProperty loadingProperty() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading.set(loading);
    }
}
