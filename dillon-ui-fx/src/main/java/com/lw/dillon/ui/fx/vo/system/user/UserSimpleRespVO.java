package com.lw.dillon.ui.fx.vo.system.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserSimpleRespVO {

    private Long id;

    private String nickname;

}
