package com.lw.dillon.ui.fx.view.system.menu;

import atlantafx.base.controls.ModalPane;
import atlantafx.base.controls.RingProgressIndicator;
import atlantafx.base.theme.Tweaks;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.lw.dillon.ui.fx.icon.WIcon;
import com.lw.dillon.ui.fx.view.control.ModalPaneDialog;
import com.lw.dillon.ui.fx.vo.system.menu.MenuRespVO;
import com.lw.dillon.ui.fx.vo.system.menu.MenuSimpleRespVO;
import de.saxsys.mvvmfx.FluentViewLoader;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import de.saxsys.mvvmfx.ViewTuple;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import org.kordamp.ikonli.feather.Feather;
import org.kordamp.ikonli.javafx.FontIcon;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ResourceBundle;

import static atlantafx.base.theme.Styles.*;

/**
 * 菜单管理视图
 *
 * @author wenli
 * @date 2023/02/15
 */
public class MenuManageView implements FxmlView<MenuManageViewModel>, Initializable {

    @InjectViewModel
    private MenuManageViewModel viewModel;


    private ModalPane modalPane;

    @FXML
    private VBox content;

    @FXML
    private StackPane root;
    private RingProgressIndicator load;

    @FXML
    private TextField searchField;

    @FXML
    private ComboBox<String> statusCombo;
    @FXML
    private Button addBut;
    @FXML
    private Button restBut;
    @FXML
    private TreeTableView<MenuRespVO> treeTableView;
    @FXML
    private TreeTableColumn<MenuRespVO, String> nameCol;
    @FXML
    private TreeTableColumn<MenuRespVO, String> iconCol;
    @FXML
    private TreeTableColumn<MenuRespVO, String> sortCol;
    @FXML
    private TreeTableColumn<MenuRespVO, String> authCol;
    @FXML
    private TreeTableColumn<MenuRespVO, String> comPathCol;
    @FXML
    private TreeTableColumn<MenuRespVO, Boolean> stateCol;
    @FXML
    private TreeTableColumn<MenuRespVO, LocalDateTime> createTime;
    @FXML
    private TreeTableColumn<MenuRespVO, String> optCol;

    @FXML
    private Button searchBut;
    @FXML
    private ToggleButton expansionBut;


    /**
     * 初始化
     *
     * @param url            url
     * @param resourceBundle 资源包
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        modalPane = new ModalPane();
        modalPane.setId("menuModalPane");
        // reset side and transition to reuse a single modal pane between different examples
        modalPane.displayProperty().addListener((obs, old, val) -> {
            if (!val) {
                modalPane.setAlignment(Pos.CENTER);
                modalPane.usePredefinedTransitionFactories(null);
            }
        });
        root.getChildren().add(modalPane);

        load = new RingProgressIndicator();
        load.disableProperty().bind(load.visibleProperty().not());
        load.visibleProperty().bindBidirectional(content.disableProperty());
        root.getChildren().add(load);

        addBut.setOnAction(event -> showEditDialog(new MenuRespVO(), false));
        addBut.getStyleClass().addAll(BUTTON_OUTLINED, ACCENT);
        searchField.textProperty().bindBidirectional(viewModel.searchTextProperty());
        statusCombo.valueProperty().bindBidirectional(viewModel.statusTextProperty());
        searchBut.setOnAction(event -> query());
        searchBut.getStyleClass().addAll(ACCENT);

        restBut.setOnAction(event -> {
            viewModel.rest();
            query();
        });
        expansionBut.selectedProperty().addListener((observable, oldValue, newValue) -> treeExpandedAll(treeTableView.getRoot(), newValue));
        nameCol.setCellValueFactory(new TreeItemPropertyValueFactory<>("name"));
        iconCol.setStyle("-fx-alignment: CENTER");
        iconCol.setCellValueFactory(new TreeItemPropertyValueFactory<>("icon"));
        iconCol.setCellFactory(new Callback<TreeTableColumn<MenuRespVO, String>, TreeTableCell<MenuRespVO, String>>() {
            @Override
            public TreeTableCell<MenuRespVO, String> call(TreeTableColumn<MenuRespVO, String> param) {
                return new TreeTableCell<>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);

                        if (StrUtil.isEmpty(item)) {
                            setGraphic(null);
                            return;
                        }

                        Label label = new Label();
                        if (StrUtil.equals("#", item)) {
                            label.setText(item);
                        } else {
                            label.setGraphic(FontIcon.of(WIcon.findByDescription("lw-" + item)));
                        }

                        setGraphic(label);
                    }
                };
            }
        });
        sortCol.setCellValueFactory(new TreeItemPropertyValueFactory<>("sort"));
        sortCol.setStyle("-fx-alignment: CENTER");
        authCol.setCellValueFactory(new TreeItemPropertyValueFactory<>("permission"));
        authCol.setStyle("-fx-alignment: CENTER");
        comPathCol.setCellValueFactory(new TreeItemPropertyValueFactory<>("path"));
        comPathCol.setStyle("-fx-alignment: CENTER");
        stateCol.setCellValueFactory(cb -> {
            var row = cb.getValue();
            var item = ObjectUtil.equal(0, row.getValue().getStatus());
            return new SimpleBooleanProperty(item);
        });
        stateCol.setStyle("-fx-alignment: CENTER");
        stateCol.setCellFactory(col -> {
            return new TreeTableCell<>() {
                @Override
                protected void updateItem(Boolean item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        var state = new Label();
                        if (item) {
                            state.setText("正常");
                            state.getStyleClass().addAll(BUTTON_OUTLINED, SUCCESS);
                        } else {
                            state.setText("停用");
                            state.getStyleClass().addAll(BUTTON_OUTLINED, DANGER);
                        }
                        HBox box = new HBox(state);
                        box.setPadding(new Insets(7, 7, 7, 7));
                        box.setAlignment(Pos.CENTER);
                        setGraphic(box);
                    }
                }
            };
        });
        createTime.setCellValueFactory(new TreeItemPropertyValueFactory<>("createTime"));
        createTime.setStyle("-fx-alignment: CENTER");
        createTime.setCellFactory(new Callback<TreeTableColumn<MenuRespVO, LocalDateTime>, TreeTableCell<MenuRespVO, LocalDateTime>>() {
            @Override
            public TreeTableCell<MenuRespVO, LocalDateTime> call(TreeTableColumn<MenuRespVO, LocalDateTime> param) {
                return new TreeTableCell<>() {
                    @Override
                    protected void updateItem(LocalDateTime item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setText(null);
                        } else {
                            if (item != null) {
                                this.setText(DateUtil.format(item, "yyyy-MM-dd HH:mm:ss"));
                            }
                        }

                    }
                };
            }
        });
        optCol.setCellValueFactory(new TreeItemPropertyValueFactory<>("name"));
        optCol.setCellFactory(new Callback<TreeTableColumn<MenuRespVO, String>, TreeTableCell<MenuRespVO, String>>() {
            @Override
            public TreeTableCell<MenuRespVO, String> call(TreeTableColumn<MenuRespVO, String> param) {

                TreeTableCell cell = new TreeTableCell<MenuRespVO, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setText(null);
                            setGraphic(null);
                        } else {

                            Button addBut = new Button("新增");
                            addBut.setOnAction(event -> showEditDialog(getTableRow().getItem(), false));
                            addBut.setGraphic(FontIcon.of(Feather.PLUS));
                            addBut.getStyleClass().addAll(FLAT, ACCENT);
                            Button editBut = new Button("修改");
                            editBut.setOnAction(event -> showEditDialog(getTableRow().getItem(), true));
                            editBut.setGraphic(FontIcon.of(Feather.EDIT));
                            editBut.getStyleClass().addAll(FLAT, ACCENT);
                            Button remBut = new Button("删除");
                            remBut.setOnAction(event -> showDelDialog(getTableRow().getItem()));
                            remBut.setGraphic(FontIcon.of(Feather.TRASH));
                            remBut.getStyleClass().addAll(FLAT, ACCENT);
                            HBox box = new HBox(addBut, editBut, remBut);
                            box.setAlignment(Pos.CENTER);
//                            box.setSpacing(7);
                            setGraphic(box);
                        }
                    }
                };
                return cell;
            }
        });

        toggleStyleClass(treeTableView, Tweaks.ALT_ICON);
        query();

    }

    /**
     * 创建树项目根
     * 创建根
     */
    private void createTreeItemRoot() {

        var root = new TreeItem<MenuRespVO>(new MenuRespVO());

        List<MenuRespVO> treeList = viewModel.getMenuList();

        if (CollUtil.isNotEmpty(treeList)) {
            treeList.forEach(MenuRespVO -> {
                var group = new TreeItem<MenuRespVO>(MenuRespVO);
                root.getChildren().add(group);
                List<MenuRespVO> children = MenuRespVO.getChildren();

                if (CollUtil.isNotEmpty(children)) {
                    generateTree(group, children);
                }
            });
        }
        treeTableView.setRoot(root);
        treeTableView.setShowRoot(false);
        root.setExpanded(true);

    }


    /**
     * 树扩展所有
     *
     * @param root     根
     * @param expanded 扩大
     */
    private void treeExpandedAll(TreeItem<MenuRespVO> root, boolean expanded) {
        for (TreeItem<MenuRespVO> child : root.getChildren()) {
            child.setExpanded(expanded);
            if (!child.getChildren().isEmpty()) {
                treeExpandedAll(child, expanded);
            }
        }
    }

    /**
     * 生成树
     *
     * @param parent   父
     * @param treeList 树列表
     */
    private void generateTree(TreeItem<MenuRespVO> parent, List<MenuRespVO> treeList) {
        treeList.forEach(treeNode -> {
            var group = new TreeItem<MenuRespVO>(treeNode);
            parent.getChildren().add(group);
            List<MenuRespVO> children = treeNode.getChildren();
            if (CollUtil.isNotEmpty(children)) {
                generateTree(group, children);
            }
        });
    }


    /**
     * 显示编辑对话框
     *
     * @param isEdit 是编辑
     */
    private void showEditDialog(MenuRespVO menuRespVO, boolean isEdit) {


        ViewTuple<MenuDialogView, MenuDialogViewModel> load = FluentViewLoader.fxmlView(MenuDialogView.class).load();
        Parent view = load.getView();
        MenuSimpleRespVO simpleRespVO = new MenuSimpleRespVO();

        if (isEdit) {
            simpleRespVO.setId(menuRespVO.getParentId());
            simpleRespVO.setName(menuRespVO.getName());
            load.getViewModel().setMenu(menuRespVO);
        } else {
            simpleRespVO.setId(menuRespVO.getId());
            simpleRespVO.setName(menuRespVO.getName());
            MenuRespVO addMenu = new MenuRespVO();
            addMenu.setId(menuRespVO.getId());
            load.getViewModel().setMenu(addMenu);
        }
        load.getViewModel().setSelectMenu(simpleRespVO);
        var dialog = new ModalPaneDialog("#menuModalPane", isEdit ? "编辑菜单" : "添加菜单",view);
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create().addRunnableInPlatformThread(() -> {
                if (isEdit) {
                    update(load.getViewModel());
                } else {
                    add(load.getViewModel());
                }
            }).onException(e -> e.printStackTrace()).run();

        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);


    }




    /**
     * 显示del对话框
     *
     * @param menuRespVO 系统菜单
     */
    private void showDelDialog(MenuRespVO menuRespVO) {

        var dialog = new ModalPaneDialog("#menuModalPane", "删除菜单",new Label("是否确认删除名称为" + menuRespVO.getName() + "的数据项？"));
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create().addRunnableInExecutor(() -> viewModel.remove(menuRespVO.getId())).addRunnableInPlatformThread(() -> {
                dialog.close();
                query();
            }).onException(e -> e.printStackTrace()).run();

        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);
    }

    /**
     * 查询
     */
    private void query() {
        ProcessChain.create().addRunnableInPlatformThread(() -> load.setVisible(true)).addRunnableInExecutor(
                () -> viewModel.query()).addRunnableInPlatformThread(() -> createTreeItemRoot()).withFinal(() -> {
            load.setVisible(false);
        }).onException(e -> e.printStackTrace()).run();
    }

    /**
     * 保存
     *
     * @param menuDialogViewModel 菜单对话框视图模型
     */
    private void update(MenuDialogViewModel menuDialogViewModel) {

        ProcessChain.create().addSupplierInExecutor(() -> menuDialogViewModel.update()).addConsumerInPlatformThread(r -> {
            if (r.isSuccess()) {
                modalPane.hide();
                query();
            }
        }).onException(e -> e.printStackTrace()).run();

    }

    private void add(MenuDialogViewModel menuDialogViewModel) {

        ProcessChain.create().addSupplierInExecutor(() -> menuDialogViewModel.add()).addConsumerInPlatformThread(r -> {
            if (r.isSuccess()) {
                modalPane.hide();
                query();
            }
        }).onException(e -> e.printStackTrace()).run();

    }




}
