package com.lw.dillon.ui.fx.view.system.role;

import atlantafx.base.controls.ToggleSwitch;
import com.lw.dillon.ui.fx.vo.system.menu.MenuSimpleRespVO;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.CheckBoxTreeCell;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * 角色分配菜单视图
 */
public class RoleAssignMenuView implements FxmlView<RoleAssignMenuViewModel>, Initializable {

    /**
     * 用于注入角色分配菜单视图模型
     */
    @InjectViewModel
    private RoleAssignMenuViewModel viewModel;

    /**
     * 用于显示按钮
     */
    @FXML
    private Button codeBut;

    /**
     * 用于菜单展开的切换开关
     */
    @FXML
    private ToggleSwitch menuExpandTs;

    /**
     * 用于显示按钮
     */
    @FXML
    private Button nameBut;

    /**
     * 用于树节点全选的切换开关
     */
    @FXML
    private ToggleSwitch treeNodeAllTs;

    /**
     * 用于显示菜单树的树视图
     */
    @FXML
    private TreeView<MenuSimpleRespVO> treeView;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        /**
         * 绑定代码But按钮的文本属性
         */
        codeBut.textProperty().bind(viewModel.codeProperty());
        /**
         * 绑定名称But按钮的文本属性
         */
        nameBut.textProperty().bind(viewModel.nameProperty());
        /**
         * 监听初始化数据事件，并执行initData方法
         */
        viewModel.subscribe("initData", (key, payload) -> initData());
        /**
         * 设置树视图不显示根节点
         */
        treeView.setShowRoot(false);
        /**
         * 设置树视图的单元格工厂
         */
        treeView.setCellFactory(tree -> new CheckBoxTreeCell<>() {
            @Override
            public void updateItem(MenuSimpleRespVO menuSimpleRespVO, boolean b) {
                super.updateItem(menuSimpleRespVO, b);
                if (b || menuSimpleRespVO == null) {
                    setText(null);
                } else {
                    setText(String.valueOf(menuSimpleRespVO.getName()));
                }
            }
        });

        //    * 监听树节点全选的切换开关的变化事件
        treeNodeAllTs.selectedProperty().addListener((observableValue, aBoolean, newVale) -> {
            treeView.getRoot().getChildren().forEach(item -> {
                if (item instanceof CheckBoxTreeItem<MenuSimpleRespVO>) {
                    ((CheckBoxTreeItem<MenuSimpleRespVO>) item).setSelected(newVale);
                }
            });
        });

        /**
         * 监听菜单展开切换开关的变化事件
         */
        menuExpandTs.selectedProperty().addListener((observableValue, aBoolean, newValue) -> {
            treeView.getRoot().getChildren().forEach(item -> {
                if (item instanceof CheckBoxTreeItem<MenuSimpleRespVO>) {
                    ((CheckBoxTreeItem<MenuSimpleRespVO>) item).setExpanded(newValue);
                }
            });
        });
    }

    /**
     * 初始化方法
     */
    private void initData() {
        createRoot();
    }

    /**
     * 创建根节点
     */
    private void createRoot() {
        /**
         * 创建根节点菜单简单响应视口实例
         */
        MenuSimpleRespVO menuSimpleRespVO = new MenuSimpleRespVO();
        menuSimpleRespVO.setName("主类目");
        menuSimpleRespVO.setId(0L);
        /**
         * 替换为实际的菜单列表
         */
        List<MenuSimpleRespVO> menuList = viewModel.getMenuList();
        /**
         * 创建复选框树节点实例作为根节点
         */
        CheckBoxTreeItem<MenuSimpleRespVO> rootItem = new CheckBoxTreeItem<>(menuSimpleRespVO);
        rootItem.setExpanded(true);
        /**
         * 递归方式构建树
         */
        buildTreeView(rootItem, menuList, 0);
        treeView.setRoot(rootItem);

    }

    /**
     * 递归方式构建树
     */
    private void buildTreeView(TreeItem<MenuSimpleRespVO> parentItem, List<MenuSimpleRespVO> menuList, long parentId) {
        for (MenuSimpleRespVO menu : menuList) {
            if (menu.getParentId() == parentId) {
                /**
                 * 创建复选框树节点实例
                 */
                CheckBoxTreeItem<MenuSimpleRespVO> item = new CheckBoxTreeItem<>(menu);
                /**
                 * 监听复选框树节点的不明确属性的变化事件
                 */
                item.indeterminateProperty().addListener((observableValue, aBoolean, t1) -> {
                    if (t1) {
                        viewModel.getSelMenuIdSet().add(item.getValue().getId());
                    }
                });
                /**
                 * 监听复选框树节点的选中属性的变化事件
                 */
                item.selectedProperty().addListener((obs, wasChecked, isNowChecked) -> {
                    if (item.isSelected()) {
                        viewModel.getSelMenuIdSet().add(item.getValue().getId());
                    } else {
                        viewModel.getSelMenuIdSet().remove(item.getValue().getId());
                    }
                });

                parentItem.getChildren().add(item);
                item.setSelected(viewModel.getSelMenuIdSet().contains(menu.getId()));
                /**
                 * 递归构建子节点
                 */
                buildTreeView(item, menuList, menu.getId());
            }
        }
    }
}
