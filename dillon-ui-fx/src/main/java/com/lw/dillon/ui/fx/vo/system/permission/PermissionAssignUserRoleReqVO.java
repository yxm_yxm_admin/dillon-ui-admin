package com.lw.dillon.ui.fx.vo.system.permission;

import lombok.Data;

import java.util.Collections;
import java.util.Set;

@Data
public class PermissionAssignUserRoleReqVO {

    private Long userId;

    private Set<Long> roleIds = Collections.emptySet(); // 兜底

}
