package com.lw.dillon.ui.fx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;


/**
 * User: hansolo
 * Date: 30.11.20
 * Time: 13:21
 */
public class CanvasTest extends Application {

    @Override
    public void init() {


    }

    @Override
    public void start(Stage stage) {
        StackPane pane = new StackPane(new CanvasControl());

        Scene scene = new Scene(pane);

        stage.setTitle("Worldmap Connections");
        stage.setScene(scene);
        stage.show();

    }

    @Override
    public void stop() {
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}