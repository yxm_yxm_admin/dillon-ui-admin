package com.lw.dillon.ui.fx.vo.system.auth;

import com.lw.dillon.framework.common.validation.Mobile;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AuthSmsLoginReqVO {

    @Mobile
    private String mobile;

    private String code;

}
