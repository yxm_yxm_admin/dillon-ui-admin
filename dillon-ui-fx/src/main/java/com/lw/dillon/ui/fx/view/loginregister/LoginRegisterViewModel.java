package com.lw.dillon.ui.fx.view.loginregister;

import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.AuthFeign;
import com.lw.dillon.ui.fx.request.feign.client.TenantFeign;
import com.lw.dillon.ui.fx.store.AppStore;
import com.lw.dillon.ui.fx.vo.system.auth.AuthLoginReqVO;
import de.saxsys.mvvmfx.SceneLifecycle;
import de.saxsys.mvvmfx.ViewModel;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * 登录注册视图模型
 *
 * @author liwen
 * @date 2022/09/25
 */
public class LoginRegisterViewModel implements ViewModel, SceneLifecycle {
    public final static String ON_VIEW_ADDEDA = "onViewAdded";
    private SimpleStringProperty userName = new SimpleStringProperty("admin");
    private SimpleStringProperty passWord = new SimpleStringProperty("admin123");
    private SimpleBooleanProperty success = new SimpleBooleanProperty(false);
    private SimpleBooleanProperty loding = new SimpleBooleanProperty(false);

    private SimpleStringProperty msg = new SimpleStringProperty();

    public void initialize() {


    }

    @Override
    public void onViewAdded() {

        publish(ON_VIEW_ADDEDA);
    }

    @Override
    public void onViewRemoved() {
    }


    public String getUserName() {
        return userName.get();
    }

    public SimpleStringProperty userNameProperty() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName.set(userName);
    }

    public String getPassWord() {
        return passWord.get();
    }

    public SimpleStringProperty passWordProperty() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord.set(passWord);
    }


    public String getMsg() {
        return msg.get();
    }

    public SimpleStringProperty msgProperty() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg.set(msg);
    }

    public boolean isSuccess() {
        return success.get();
    }

    public SimpleBooleanProperty successProperty() {
        return success;
    }


    public boolean isLoding() {
        return loding.get();
    }

    public SimpleBooleanProperty lodingProperty() {
        return loding;
    }

    public void setLoding(boolean loding) {
        this.loding.set(loding);
    }

    /**
     * 登录
     */
    public void login() {

        AuthLoginReqVO authLoginReqVO = new AuthLoginReqVO();
        authLoginReqVO.setUsername(userName.getValue());
        authLoginReqVO.setPassword(passWord.getValue());
        loding.setValue(true);
        ProcessChain.create()
//                .addSupplierInExecutor(() -> Request.connector(TenantFeign.class).getTenantIdByName("芋道源码"))
//                .addConsumerInPlatformThread(rel -> AppStore.setTenantId(rel.getData()))
                .addSupplierInExecutor(() -> Request.connector(AuthFeign.class).login(authLoginReqVO))
                .addConsumerInPlatformThread(rel -> {
                    if (rel.isSuccess()) {
                        AppStore.setToken(rel.getData().getAccessToken());
                        success.setValue(true);
                    } else {
                        msg.setValue(rel.getMsg());
                    }
                }).withFinal(() -> loding.setValue(false))
                .run();

    }

}
