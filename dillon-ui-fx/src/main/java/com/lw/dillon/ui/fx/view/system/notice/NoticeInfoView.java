package com.lw.dillon.ui.fx.view.system.notice;

import cn.hutool.core.util.NumberUtil;
import com.lw.dillon.ui.fx.enums.DictTypeEnum;
import com.lw.dillon.ui.fx.store.DictStore;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataSimpleRespVO;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.web.HTMLEditor;
import javafx.util.StringConverter;

import java.net.URL;
import java.util.ResourceBundle;

public class NoticeInfoView implements FxmlView<NoticeInfoViewModel>, Initializable {

    @InjectViewModel
    private NoticeInfoViewModel viewModel;

    @FXML
    private HTMLEditor contentHtmlEditor;

    @FXML
    private TextArea remarkCombox;

    @FXML
    private ComboBox<Integer> statusComboBox;

    @FXML
    private TextField titleTexField;

    @FXML
    private ComboBox<DictDataSimpleRespVO> typeComboBox;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        remarkCombox.textProperty().bindBidirectional(viewModel.remarkProperty());
        titleTexField.textProperty().bindBidirectional(viewModel.titleProperty());
        statusComboBox.valueProperty().bindBidirectional(viewModel.statusProperty());
        statusComboBox.setConverter(new StringConverter<Integer>() {
            @Override
            public String toString(Integer integer) {
                if (integer != null) {
                    return integer == 0 ? "开启" : "关闭";
                }
                return null;
            }

            @Override
            public Integer fromString(String s) {
                return null;
            }
        });
        typeComboBox.setItems(FXCollections.observableArrayList(DictStore.getDictDataMap(DictTypeEnum.SYSTEM_NOTICE_TYPE.getDictType()).values()));
        typeComboBox.setConverter(new StringConverter<DictDataSimpleRespVO>() {
            @Override
            public String toString(DictDataSimpleRespVO dictDataSimpleRespVO) {
                if (dictDataSimpleRespVO != null) {
                    return dictDataSimpleRespVO.getLabel();
                }
                return null;
            }

            @Override
            public DictDataSimpleRespVO fromString(String s) {
                return null;
            }
        });
        viewModel.contentProperty().addListener((observable, oldValue, newValue) -> {
            contentHtmlEditor.setHtmlText(newValue);
        });
        viewModel.subscribe("commitHtmlText", (key, payload) -> {
            viewModel.contentProperty().setValue(contentHtmlEditor.getHtmlText());
        });

        initListeners();
    }

    private void initListeners() {

        typeComboBox.getSelectionModel().selectedItemProperty().addListener((observableValue, dictDataSimpleRespVO, t1) -> {
                    if (t1 != null) {
                        viewModel.typeProperty().set(NumberUtil.parseInt(t1.getValue()));
                    }

                }
        );
        viewModel.typeProperty().addListener((observableValue, integer, t1) -> {

            DictDataSimpleRespVO simpleRespVO = DictStore.getDictDataMap(DictTypeEnum.SYSTEM_NOTICE_TYPE.getDictType()).get(t1.toString());

            typeComboBox.getSelectionModel().select(simpleRespVO);

        });

    }

    public void saveHtmlText() {
        viewModel.contentProperty().set(contentHtmlEditor.getHtmlText());
    }

}
