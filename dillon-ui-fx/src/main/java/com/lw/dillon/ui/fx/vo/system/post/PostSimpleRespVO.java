package com.lw.dillon.ui.fx.vo.system.post;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostSimpleRespVO {

    private final BooleanProperty state = new SimpleBooleanProperty(false);

    private Long id;

    private String name;


}
