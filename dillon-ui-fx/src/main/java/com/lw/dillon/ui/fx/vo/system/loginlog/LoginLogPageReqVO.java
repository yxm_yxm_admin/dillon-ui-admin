package com.lw.dillon.ui.fx.vo.system.loginlog;

import com.lw.dillon.framework.common.pojo.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
public class LoginLogPageReqVO extends PageParam {

    private String userIp;

    private String username;

    private Boolean status;

    private LocalDateTime[] createTime;

}
