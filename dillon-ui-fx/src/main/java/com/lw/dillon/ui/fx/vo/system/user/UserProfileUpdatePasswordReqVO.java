package com.lw.dillon.ui.fx.vo.system.user;

import lombok.Data;

@Data
public class UserProfileUpdatePasswordReqVO {

    private String oldPassword;

    private String newPassword;

}
