package com.lw.dillon.ui.fx.view.system.logininfor;

import atlantafx.base.controls.ModalPane;
import atlantafx.base.controls.RingProgressIndicator;
import cn.hutool.core.date.DateUtil;
import com.lw.dillon.ui.fx.util.NodeUtils;
import com.lw.dillon.ui.fx.view.control.ModalPaneDialog;
import com.lw.dillon.ui.fx.view.control.PagingControl;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.loginlog.LoginLogRespVO;
import de.saxsys.mvvmfx.FluentViewLoader;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import de.saxsys.mvvmfx.ViewTuple;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.kordamp.ikonli.feather.Feather;
import org.kordamp.ikonli.javafx.FontIcon;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

import static atlantafx.base.theme.Styles.ACCENT;
import static atlantafx.base.theme.Styles.FLAT;
import static atlantafx.base.theme.Tweaks.*;

public class LoginInforView implements FxmlView<LoginInforViewModel>, Initializable {

    @InjectViewModel
    private LoginInforViewModel viewModel;


    @FXML
    private TableColumn<LoginLogRespVO, Integer> resultCol;

    @FXML
    private VBox contentPane;

    @FXML
    private TableColumn<LoginLogRespVO, LocalDateTime> createTimeCol;

    @FXML
    private Button delBut;

    @FXML
    private Button emptyBut;

    @FXML
    private DatePicker endDatePicker;

    @FXML
    private TableColumn<LoginLogRespVO, Long> idCol;

    @FXML
    private TextField ipaddrField;

    @FXML
    private TableColumn<LoginLogRespVO, Integer> logTypeCol;


    @FXML
    private Button resetBut;

    @FXML
    private StackPane rootPane;

    @FXML
    private Button searchBut;

    @FXML
    private DatePicker startDatePicker;

    @FXML
    private ComboBox<Boolean> statusCombo;

    @FXML
    private TableView<LoginLogRespVO> tableView;

    @FXML
    private TableColumn<LoginLogRespVO, String> userAgentCol;

    @FXML
    private TableColumn<LoginLogRespVO, String> userIpCol;

    @FXML
    private TextField userNameField;

    @FXML
    private TableColumn<LoginLogRespVO, String> usernameCol;
    @FXML
    private TableColumn<LoginLogRespVO, String> optCol;

    private RingProgressIndicator loading;

    private PagingControl pagingControl;


    private ModalPane modalPane;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        modalPane = new ModalPane();
        modalPane.setId("logininforModalPane");
        // reset side and transition to reuse a single modal pane between different examples
        modalPane.displayProperty().addListener((obs, old, val) -> {
            if (!val) {
                modalPane.setAlignment(Pos.CENTER);
                modalPane.usePredefinedTransitionFactories(null);
            }
        });
        rootPane.getChildren().add(modalPane);

        pagingControl = new PagingControl();
        contentPane.getChildren().add(pagingControl);
        pagingControl.totalProperty().bind(viewModel.totalProperty());
        viewModel.pageNumProperty().bind(pagingControl.pageNumProperty());
        viewModel.pageSizeProperty().bindBidirectional(pagingControl.pageSizeProperty());
        viewModel.pageNumProperty().addListener((observable, oldValue, newValue) -> {
            viewModel.queryLogininforList();
        });
        pagingControl.pageSizeProperty().addListener((observable, oldValue, newValue) -> {
            viewModel.queryLogininforList();
        });

        loading = new RingProgressIndicator();
        loading.disableProperty().bind(loading.visibleProperty().not());
        loading.visibleProperty().bindBidirectional(contentPane.disableProperty());
        contentPane.disableProperty().bind(viewModel.lodingProperty());

        rootPane.getChildren().add(loading);

        ipaddrField.textProperty().bindBidirectional(viewModel.ipaddrProperty());
        userNameField.textProperty().bindBidirectional(viewModel.userNameProperty());
        statusCombo.valueProperty().bindBidirectional(viewModel.statusProperty());
        startDatePicker.valueProperty().bindBidirectional(viewModel.startDateProperty());
        endDatePicker.valueProperty().bindBidirectional(viewModel.endDateProperty());
        searchBut.setOnAction(event -> viewModel.queryLogininforList());
        searchBut.getStyleClass().addAll(ACCENT);

        resetBut.setOnAction(event -> viewModel.reset());

        delBut.setOnAction(event -> {

        });
        emptyBut.setOnAction(event -> {
        });

        statusCombo.setConverter(new StringConverter<Boolean>() {
            @Override
            public String toString(Boolean aBoolean) {
                return aBoolean ? "成功" : "失败";
            }

            @Override
            public Boolean fromString(String s) {
                return null;
            }
        });

        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        logTypeCol.setCellValueFactory(new PropertyValueFactory<>("logType"));
        usernameCol.setCellValueFactory(new PropertyValueFactory<>("username"));
        userIpCol.setCellValueFactory(new PropertyValueFactory<>("userIp"));
        userAgentCol.setCellValueFactory(new PropertyValueFactory<>("userAgent"));
        resultCol.setCellValueFactory(new PropertyValueFactory<>("result"));
        createTimeCol.setCellValueFactory(new PropertyValueFactory<>("createTime"));

        resultCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        var state = new Label();
                        DictDataSimpleRespVO dictDataSimpleRespVO = viewModel.getResultMap().get(item + "");
                        state.setText(dictDataSimpleRespVO.getLabel());
                        state.getStyleClass().addAll(dictDataSimpleRespVO.getColorType());
                        HBox box = new HBox(state);
                        box.setPadding(new Insets(7, 7, 7, 7));
                        box.setAlignment(Pos.CENTER);
                        setGraphic(box);
                    }
                }
            };
        });

        logTypeCol.setCellFactory(col -> new TableCell<>() {
            @Override
            protected void updateItem(Integer item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    var state = new Label();

                    DictDataSimpleRespVO dictDataSimpleRespVO = viewModel.getTypeMap().get(item + "");
                    state.setText(dictDataSimpleRespVO.getLabel());
                    state.getStyleClass().addAll(dictDataSimpleRespVO.getColorType());
                    HBox box = new HBox(state);
                    box.setPadding(new Insets(7, 7, 7, 7));
                    box.setAlignment(Pos.CENTER);
                    setGraphic(box);
                }
            }
        });

        optCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                    } else {

                        Button editBut = new Button("详细");
                        editBut.setOnAction(event -> showDictDataInfoDialog(getTableRow().getItem()));
                        editBut.setGraphic(FontIcon.of(Feather.EYE));
                        editBut.getStyleClass().addAll(FLAT, ACCENT);

                        HBox box = new HBox(editBut);
                        box.setAlignment(Pos.CENTER);
//                            box.setSpacing(7);
                        setGraphic(box);
                    }
                }
            };
        });

        createTimeCol.setCellFactory(new Callback<TableColumn<LoginLogRespVO, LocalDateTime>, TableCell<LoginLogRespVO, LocalDateTime>>() {
            @Override
            public TableCell<LoginLogRespVO, LocalDateTime> call(TableColumn<LoginLogRespVO, LocalDateTime> param) {
                return new TableCell<>() {
                    @Override
                    protected void updateItem(LocalDateTime item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setText(null);
                        } else {
                            if (item != null) {
                                this.setText(DateUtil.format(item, "yyyy-MM-dd HH:mm:ss"));
                            }
                        }

                    }
                };
            }
        });

        tableView.setItems(viewModel.getLoginLogList());
        tableView.getSelectionModel().setCellSelectionEnabled(false);
        for (TableColumn c : tableView.getColumns()) {
            NodeUtils.addStyleClass(c, ALIGN_CENTER, ALIGN_LEFT, ALIGN_RIGHT);
        }


    }


    private void showDictDataInfoDialog(LoginLogRespVO sysOperLog) {

        ViewTuple<LoginInforInfoView, LoginInforInfoViewModel> load = FluentViewLoader.fxmlView(LoginInforInfoView.class).load();
        load.getViewModel().setLoginLogRespVO(sysOperLog);

        var dialog = new ModalPaneDialog("#logininforModalPane", "登录日志详细", load.getView());
        var closeBtn = new Button("关闭");
        closeBtn.setOnAction(e->dialog.close());
        HBox box = new HBox(10, closeBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);
    }


}
