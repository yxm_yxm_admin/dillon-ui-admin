package com.lw.dillon.ui.fx.vo.system.post;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
public class PostRespVO extends PostBaseVO {

    private Long id;

    private LocalDateTime createTime;

}
