package com.lw.dillon.ui.fx.vo.system.user;

import lombok.Data;

@Data
public class UserUpdatePasswordReqVO {

    private Long id;

    private String password;

}
