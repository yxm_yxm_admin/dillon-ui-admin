package com.lw.dillon.ui.fx.vo.system.post;

import lombok.Data;

@Data
public class PostExportReqVO {

    private String code;

    private String name;

    private Integer status;

}
