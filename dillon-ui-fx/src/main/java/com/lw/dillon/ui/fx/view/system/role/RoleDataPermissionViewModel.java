package com.lw.dillon.ui.fx.view.system.role;

import cn.hutool.core.collection.CollUtil;
import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.PermissionFeign;
import com.lw.dillon.ui.fx.request.feign.client.SysDeptFeign;
import com.lw.dillon.ui.fx.request.feign.client.SysRoleFeign;
import com.lw.dillon.ui.fx.store.DictStore;
import com.lw.dillon.ui.fx.vo.system.dept.DeptSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.permission.PermissionAssignRoleDataScopeReqVO;
import com.lw.dillon.ui.fx.vo.system.role.RoleDO;
import de.saxsys.mvvmfx.ViewModel;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;

import java.util.Map;

public class RoleDataPermissionViewModel implements ViewModel {

    // 角色名称
    private StringProperty name = new SimpleStringProperty();
    // 角色编码
    private StringProperty code = new SimpleStringProperty();
    private IntegerProperty dataScope = new SimpleIntegerProperty(0);
    // 角色ID
    private ObjectProperty<Long> id = new SimpleObjectProperty<>(null);

    private ObservableList<DeptSimpleRespVO> deptList = FXCollections.observableArrayList();
    private ObservableList<DictDataSimpleRespVO> dictDataSimpleRespVOS = FXCollections.observableArrayList();

    private ObservableSet<Long> dataScopeDeptIds = FXCollections.observableSet();

    public RoleDataPermissionViewModel() {

        Map<String, DictDataSimpleRespVO> stringDictDataSimpleRespVOMap = DictStore.getDictDataMap("system_data_scope");

        stringDictDataSimpleRespVOMap.forEach((key, value) -> {
            dictDataSimpleRespVOS.add(value);
        });

    }

    public void initializeData(RoleDO roleDO) {
        this.id.set(roleDO.getId());
        this.name.set(roleDO.getName());
        this.code.set(roleDO.getCode());
        this.dataScope.set(roleDO.getDataScope());

        ProcessChain.create()
                .addSupplierInExecutor(() -> Request.connector(SysDeptFeign.class).getSimpleDeptList().getCheckedData())
                .addConsumerInExecutor(rel -> deptList.addAll(rel))
                .addSupplierInExecutor(() -> Request.connector(SysRoleFeign.class).getRole(id.get()).getCheckedData())
                .addConsumerInPlatformThread(respVO -> {
                    if (CollUtil.isNotEmpty(respVO.getDataScopeDeptIds())) {
                        dataScopeDeptIds.addAll(respVO.getDataScopeDeptIds());

                    }
                })
                .onException(throwable -> throwable.printStackTrace())
                .withFinal(() -> publish("initData"))
                .run();
    }

    public CommonResult<Boolean> assignUserRole() {
        PermissionAssignRoleDataScopeReqVO reqVO = new PermissionAssignRoleDataScopeReqVO();
        reqVO.setRoleId(id.get());
        reqVO.setDataScope(dataScope.get());
        reqVO.setDataScopeDeptIds(dataScopeDeptIds);
        return Request.connector(PermissionFeign.class).assignRoleDataScope(reqVO);

    }


    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getCode() {
        return code.get();
    }

    public StringProperty codeProperty() {
        return code;
    }

    public void setCode(String code) {
        this.code.set(code);
    }

    public Long getId() {
        return id.get();
    }

    public ObjectProperty<Long> idProperty() {
        return id;
    }

    public void setId(Long id) {
        this.id.set(id);
    }

    public ObservableList<DeptSimpleRespVO> getDeptList() {
        return deptList;
    }

    public void setDeptList(ObservableList<DeptSimpleRespVO> deptList) {
        this.deptList = deptList;
    }

    public ObservableList<DictDataSimpleRespVO> getDictDataSimpleRespVOS() {
        return dictDataSimpleRespVOS;
    }

    public int getDataScope() {
        return dataScope.get();
    }

    public IntegerProperty dataScopeProperty() {
        return dataScope;
    }

    public void setDataScope(int dataScope) {
        this.dataScope.set(dataScope);
    }

    public ObservableSet<Long> getDataScopeDeptIds() {
        return dataScopeDeptIds;
    }


}
