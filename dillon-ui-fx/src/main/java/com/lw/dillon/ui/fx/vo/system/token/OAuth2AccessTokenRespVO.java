package com.lw.dillon.ui.fx.vo.system.token;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OAuth2AccessTokenRespVO {

    private Long id;

    private String accessToken;

    private String refreshToken;

    private Long userId;

    private Integer userType;

    private String clientId;

    private LocalDateTime createTime;

    private LocalDateTime expiresTime;

}
